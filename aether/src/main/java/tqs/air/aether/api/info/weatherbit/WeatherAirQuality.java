package tqs.air.aether.api.info.weatherbit;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import tqs.air.aether.api.info.AirQualityItem;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherAirQuality implements AirQualityItem {
    private int aqi;
    private double o3;
    private double so2;
    private double no2;
    private double co;
    private double pm10;
    private double pm25;

    private String date;
}
