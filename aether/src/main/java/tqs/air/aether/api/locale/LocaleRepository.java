package tqs.air.aether.api.locale;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface LocaleRepository extends JpaRepository<Locale, String> {
    public Locale findByName(String name);
    public List<Locale> findAll();
}
