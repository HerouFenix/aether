package tqs.air.aether.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import tqs.air.aether.api.cache.LocalCache;
import tqs.air.aether.api.info.AirQualityItem;
import tqs.air.aether.api.info.AirResponse;
import tqs.air.aether.api.info.Coordinates;
import tqs.air.aether.api.info.breeze.BreezeAirQuality;
import tqs.air.aether.api.info.breeze.Pollutant;
import tqs.air.aether.api.info.weatherbit.WeatherAirQuality;
import tqs.air.aether.api.locale.Locale;
import tqs.air.aether.api.locale.LocalePOJO;
import tqs.air.aether.api.locale.LocaleRepository;

import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@NoArgsConstructor
public class AetherService {
    @Autowired
    private LocaleRepository localeRepository;

    @Autowired
    private LocalCache localCache;

    @Value("${BREEZE_API_URL}")
    private String breezeURL;

    @Value("${BREEZE_API_TOKEN}")
    private String breezeToken;

    @Value("${WEATHERBIT_API_URL}")
    private String weatherURL;

    @Value("${WEATHERBIT_API_TOKEN}")
    private String weatherToken;

    private static final String BREEZE_FEATURES = "&features=breezometer_aqi,pollutants_concentrations,pollutants_aqi_information";

    private static final String LON = "&lon=";
    private static final String LAT = "&lat=";
    private static final String KEY = "&key=";


    private static final String CURRENT = "CURRENT";
    private static final String FUTURE = "FUTURE";

    private RestTemplate restTemplate = new RestTemplate();

    public AetherService(String breezeURL, String breezeToken, String weatherURL, String weatherToken) {
        this.breezeURL = breezeURL;
        this.breezeToken = breezeToken;
        this.weatherURL = weatherURL;
        this.weatherToken = weatherToken;
    }


    public Locale createLocale(LocalePOJO newLocale) {
        if (localeRepository.findByName(newLocale.getName()) != null) {
            return null;
        }

        Locale locale = new Locale();
        locale.setName(newLocale.getName());
        locale.setLat(newLocale.getLat());
        locale.setLon(newLocale.getLon());

        return localeRepository.save(locale);
    }

    public Locale updateLocale(LocalePOJO updatedLocale) {
        Locale locale = localeRepository.findByName(updatedLocale.getName());

        if (locale == null) {
            return null;
        }

        locale.setName(updatedLocale.getName());
        locale.setLat(updatedLocale.getLat());
        locale.setLon(updatedLocale.getLon());

        return localeRepository.save(locale);
    }

    public Locale deleteLocale(String localeName) {
        Locale locale = localeRepository.findByName(localeName);

        if (locale == null) {
            return null;
        }

        localeRepository.delete(locale);

        return locale;
    }

    public List<Locale> getAllLocales() {
        return this.localeRepository.findAll();
    }

    public List<AirResponse> getEntireCache() {
        List<AirResponse> returnVal = new ArrayList<>();

        for (AirResponse item : this.localCache.getCache().values()) {
            returnVal.add(item);
        }
        return returnVal;
    }

    public Integer getCacheSize() {
        return this.localCache.getSize();
    }

    public Map<String, Integer> getHitsNMisses() {
        Map<String, Integer> hitsnmisses = new HashMap<>();
        hitsnmisses.put("Hits", this.localCache.getHits());
        hitsnmisses.put("Misses", this.localCache.getMisses());
        hitsnmisses.put("TotalRequests", this.localCache.getTotalRequests());

        return hitsnmisses;
    }

    public AirResponse getQualityByLocale(String localeName, String reqType, @Nullable String api) throws JsonProcessingException {
        Locale locale = localeRepository.findByName(localeName);

        if (locale == null) {
            return null;
        }

        return getQualityByCoord(locale.getLat(), locale.getLon(), reqType, api, localeName);
    }

    public AirResponse getQualityByCoord(double lat, double lon, String reqType, @Nullable String api, @Nullable String locale) throws JsonProcessingException {
        AirResponse result;
        Coordinates coords = new Coordinates(lat, lon);
        LocalDateTime reqDate = LocalDateTime.now();

        if (locale != null) {
            coords.setLocale(locale);
        }

        // Try to get the result from the cache
        result = this.localCache.getFromCache(coords, api, reqType);

        // If we managed to get the result from the cache, and the result comes from the api we want return it
        if (result != null) {
            return result;
        }

        // In case no API is specified, try to get the result from breezeometer, and if it fails, from weatherbit
        if (api == null) {
            result = this.getQualityNullAPI(coords, reqType);

        } else {
            result = this.getQualityWithAPI(coords, reqType, api);
        }

        result.setCoordinates(coords);
        result.setRequestDate(reqDate);

        this.localCache.addToCache(result, reqType);
        return result;
    }

    private AirResponse getQualityNullAPI(Coordinates coordinates, String reqType) throws JsonProcessingException {
        AirResponse result;

        try {
            result = new AirResponse();
            if (reqType.equals(CURRENT)) {
                result.addToData(this.callBreezeAPICur(coordinates.getLat(), coordinates.getLon()), 0);
            } else {
                result.setData(this.callBreezeAPIFuturePast(coordinates.getLat(), coordinates.getLon(), reqType));
            }
            result.setApiName("Breeze");

        } catch (RestClientResponseException e1) {
            JsonNode jsonNodeBreeze = new ObjectMapper().readTree(e1.getResponseBodyAsString());
            String breezeError = jsonNodeBreeze.get("error").get("detail").textValue();

            try {
                result = new AirResponse();
                if (reqType.equals(CURRENT)) {
                    result.addToData(this.callWeatherAPICur(coordinates.getLat(), coordinates.getLon()), 0);
                } else {
                    result.setData(this.callWeatherAPIFuturePast(coordinates.getLat(), coordinates.getLon(), reqType));
                }
                result.setApiName("Weather");

            } catch (RestClientResponseException e2) {
                JsonNode jsonNodeWeather = new ObjectMapper().readTree(e2.getResponseBodyAsString());
                String weatherError = jsonNodeWeather.get("error").textValue();

                String fullError = "{\"error\": \"Both APIs responded with errors - Breeze: " + breezeError + " ; WeatherBit: " + weatherError + "\"}";
                throw new RestClientResponseException(e2.getMessage(), e2.getRawStatusCode(), e2.getStatusText(), e2.getResponseHeaders(), fullError.getBytes(), Charset.defaultCharset());
            }
        }

        return result;
    }

    private AirResponse getQualityWithAPI(Coordinates coordinates, String reqType, String api) {
        AirResponse result;

        result = new AirResponse();
        result.setApiName(api);

        switch (api) {
            case "Breeze":
                if (reqType.equals(CURRENT)) {
                    result.addToData(this.callBreezeAPICur(coordinates.getLat(), coordinates.getLon()), 0);
                } else {
                    result.setData(this.callBreezeAPIFuturePast(coordinates.getLat(), coordinates.getLon(), reqType));
                }
                break;
            case "Weather":
                if (reqType.equals(CURRENT)) {
                    result.addToData(this.callWeatherAPICur(coordinates.getLat(), coordinates.getLon()), 0);
                } else {
                    result.setData(this.callWeatherAPIFuturePast(coordinates.getLat(), coordinates.getLon(), reqType));
                }
                break;
            default:
                throw new IllegalStateException("Unexpected API value : " + api + " . Was expecting 'Breeze', 'Weather' or nothing");
        }

        return result;
    }


    private BreezeAirQuality callBreezeAPICur(double lat, double lon) {
        String requestURL = this.breezeURL + "/current-conditions?" + LAT + lat + LON + lon + KEY + this.breezeToken + BREEZE_FEATURES;

        JsonNode jsonNode = restTemplate.getForObject(requestURL, JsonNode.class);

        BreezeAirQuality result = new BreezeAirQuality();

        result.setBaqi(jsonNode.get("data").get("indexes").get("baqi").get("aqi").intValue());
        result.setDate(LocalDateTime.now().toString());

        for (JsonNode pollutantNode : jsonNode.get("data").get("pollutants")) {
            Pollutant newPol = new Pollutant();
            newPol.setName(pollutantNode.get("full_name").textValue());
            newPol.setAqi(pollutantNode.get("aqi_information").get("baqi").get("aqi").intValue());

            JsonNode concentration = pollutantNode.get("concentration");

            newPol.setConcentration(concentration.get("value").doubleValue());
            newPol.setUnit(concentration.get("units").textValue());

            String acronym = pollutantNode.get("display_name").textValue();

            result.addPollutant(acronym, newPol);
        }

        return result;
    }

    private WeatherAirQuality callWeatherAPICur(double lat, double lon) {
        String requestURL = this.weatherURL + "/current/airquality?" + LAT + lat + LON + lon + KEY + this.weatherToken;

        JsonNode jsonNode = restTemplate.getForObject(requestURL, JsonNode.class);

        WeatherAirQuality result = new WeatherAirQuality();
        result.setDate(LocalDateTime.now().toString());

        result.setAqi(jsonNode.get("data").get(0).get("aqi").intValue());
        result.setO3(jsonNode.get("data").get(0).get("o3").doubleValue());
        result.setSo2(jsonNode.get("data").get(0).get("so2").doubleValue());
        result.setNo2(jsonNode.get("data").get(0).get("no2").doubleValue());
        result.setCo(jsonNode.get("data").get(0).get("co").doubleValue());
        result.setPm10(jsonNode.get("data").get(0).get("pm10").doubleValue());
        result.setPm25(jsonNode.get("data").get(0).get("pm25").doubleValue());

        return result;
    }

    private Map<Integer, AirQualityItem> callBreezeAPIFuturePast(double lat, double lon, String type) {
        Map<Integer, AirQualityItem> finalResult = new HashMap<>();
        String requestURL = null;
        LocalDateTime current = LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT);

        for (int i = 1; i < 4; i++) {
            if (type.equals(FUTURE)) {
                String date = current.plusDays(i).format(DateTimeFormatter.ISO_DATE_TIME);
                requestURL = this.breezeURL + "/forecast/hourly?" + LAT + lat + LON + lon + KEY + this.breezeToken + "&datetime=" + date + BREEZE_FEATURES;
            } else {
                String date = current.minusDays(i).format(DateTimeFormatter.ISO_DATE_TIME);
                requestURL = this.breezeURL + "/historical/hourly?" + LAT + lat + LON + lon + KEY + this.breezeToken + "&datetime=" + date + BREEZE_FEATURES;
            }

            JsonNode jsonNode = restTemplate.getForObject(requestURL, JsonNode.class);

            JsonNode airData = jsonNode.get("data");

            BreezeAirQuality result = new BreezeAirQuality();
            result.setDate(airData.get("datetime").textValue());

            result.setBaqi(airData.get("indexes").get("baqi").get("aqi").intValue());

            for (JsonNode pollutantNode : airData.get("pollutants")) {
                Pollutant newPol = new Pollutant();
                newPol.setName(pollutantNode.get("full_name").textValue());
                newPol.setAqi(pollutantNode.get("aqi_information").get("baqi").get("aqi").intValue());

                JsonNode concentration = pollutantNode.get("concentration");

                newPol.setConcentration(concentration.get("value").doubleValue());
                newPol.setUnit(concentration.get("units").textValue());

                String acronym = pollutantNode.get("display_name").textValue();

                result.addPollutant(acronym, newPol);
            }

            finalResult.put(i - 1, result);
        }


        return finalResult;
    }

    private Map<Integer, AirQualityItem> callWeatherAPIFuturePast(double lat, double lon, String type) {
        Map<Integer, AirQualityItem> finalResult = new HashMap<>();
        String requestURL = null;
        if (type.equals(FUTURE)) {
            requestURL = this.weatherURL + "/forecast/airquality?" + LAT + lat + LON + lon + KEY + this.weatherToken;
        } else {
            requestURL = this.weatherURL + "/history/airquality?" + LAT + lat + LON + lon + KEY + this.weatherToken;
        }

        JsonNode jsonNode = restTemplate.getForObject(requestURL, JsonNode.class);

        ArrayNode airData = (ArrayNode) jsonNode.get("data");

        for (int i = 0; i < airData.size(); i++) {
            WeatherAirQuality result = new WeatherAirQuality();
            result.setDate(airData.get(i).get("timestamp_utc").textValue());

            result.setAqi(airData.get(i).get("aqi").intValue());
            result.setO3(airData.get(i).get("o3").doubleValue());
            result.setSo2(airData.get(i).get("so2").doubleValue());
            result.setNo2(airData.get(i).get("no2").doubleValue());
            result.setCo(airData.get(i).get("co").doubleValue());
            result.setPm10(airData.get(i).get("pm10").doubleValue());
            result.setPm25(airData.get(i).get("pm25").doubleValue());

            finalResult.put(i, result);
        }

        return finalResult;
    }


}
