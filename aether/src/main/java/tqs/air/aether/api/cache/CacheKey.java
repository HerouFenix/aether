package tqs.air.aether.api.cache;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import tqs.air.aether.api.info.Coordinates;

@Getter
@Setter
@EqualsAndHashCode
public class CacheKey {
    private Coordinates coords;
    private String api;
    private String reqTime;

    public CacheKey(Coordinates coords, String api, String reqTime) {
        this.coords = coords;
        this.api = api;
        this.reqTime = reqTime;
    }
}
