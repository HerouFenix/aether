package tqs.air.aether.api.locale;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocalePOJO {
    private String name;

    private double lat;

    private double lon;

    public LocalePOJO(String name, double lat, double lon) {
        this.name = name;
        this.lat = lat;
        this.lon = lon;
    }
}
