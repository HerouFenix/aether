package tqs.air.aether.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.server.ResponseStatusException;

import tqs.air.aether.api.info.AirResponse;
import tqs.air.aether.api.locale.Locale;
import tqs.air.aether.api.locale.LocalePOJO;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api")
public class AetherRestController {
    private static final String CURRENT = "CURRENT";
    private static final String FUTURE = "FUTURE";
    private static final String PAST = "PAST";


    @Autowired
    private AetherService aetherService;

    @PostMapping(value = "/locale")
    public ResponseEntity<Locale> createLocale(@RequestBody LocalePOJO locale) {
        Locale response = aetherService.createLocale(locale);

        if (response == null) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "Error! There already exists a locale named: " + locale.getName());
        }

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping(value = "/locale")
    public ResponseEntity<Locale> updateLocale(@RequestBody LocalePOJO locale) {
        Locale response = aetherService.updateLocale(locale);

        if (response == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Error! Couldn't find any locale named: " + locale.getName());
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/locale")
    public ResponseEntity<Locale> deleteLocale(@RequestParam(name = "locale") String locale) {

        Locale response = aetherService.deleteLocale(locale);

        if (response == null) {
            throw handleNoLocaleException();
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/locale")
    public ResponseEntity<List<Locale>> getAllLocales() {
        return new ResponseEntity<>(aetherService.getAllLocales(), HttpStatus.OK);
    }

    @GetMapping(value = "/cache/items")
    public ResponseEntity<List<AirResponse>> getEntireCache() {
        return new ResponseEntity<>(aetherService.getEntireCache(), HttpStatus.OK);
    }

    @GetMapping(value = "/cache/size")
    public ResponseEntity<Integer> getCacheSize() {
        return new ResponseEntity<>(aetherService.getCacheSize(), HttpStatus.OK);
    }

    @GetMapping(value = "/cache/hitsnmisses")
    public ResponseEntity<Map<String, Integer>> getHitsNMisses() {
        return new ResponseEntity<>(aetherService.getHitsNMisses(), HttpStatus.OK);
    }

    @GetMapping(value = "/air", params = {"lat", "lon"})
    public ResponseEntity<AirResponse> getAirQualityByCoord(@RequestParam(name = "lat") double lat, @RequestParam(name = "lon") double lon, @RequestParam(name = "api", required = false) String api){
        return this.qualityByCoord(lat, lon, api, CURRENT);
    }

    @GetMapping(value = "/air", params = {"locale"})
    public ResponseEntity<AirResponse> getAirQualityByLocale(@RequestParam(name = "locale") String locale, @RequestParam(name = "api", required = false) String api){
        return this.qualityByLocale(locale, api, CURRENT);
    }

    @GetMapping(value = "/air/past", params = {"lat", "lon"})
    public ResponseEntity<AirResponse> getAirQualityByCoordPast(@RequestParam(name = "lat") double lat, @RequestParam(name = "lon") double lon, @RequestParam(name = "api", required = false) String api){
        return this.qualityByCoord(lat, lon, api, PAST);
    }

    @GetMapping(value = "/air/past", params = {"locale"})
    public ResponseEntity<AirResponse> getAirQualityByLocalePast(@RequestParam(name = "locale") String locale, @RequestParam(name = "api", required = false) String api){
        return this.qualityByLocale(locale, api, PAST);
    }

    @GetMapping(value = "/air/future", params = {"lat", "lon"})
    public ResponseEntity<AirResponse> getAirQualityByCoordFuture(@RequestParam(name = "lat") double lat, @RequestParam(name = "lon") double lon, @RequestParam(name = "api", required = false) String api){
        return this.qualityByCoord(lat, lon, api, FUTURE);
    }

    @GetMapping(value = "/air/future", params = {"locale"})
    public ResponseEntity<AirResponse> getAirQualityByLocaleFuture(@RequestParam(name = "locale") String locale, @RequestParam(name = "api", required = false) String api){
        return this.qualityByLocale(locale, api, FUTURE);
    }

    // Method used to throw an appropriate error when the API returns an error
    public ResponseStatusException handleAPIException(RestClientResponseException e) {
        JsonNode jsonNode;
        try {
            jsonNode = new ObjectMapper().readTree(e.getResponseBodyAsString());
        } catch (Exception unexpected) {
            return new ResponseStatusException(
                    HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }

        String errorMessage;

        //Special error that is thrown when the Weatherbit api key expires
        if(jsonNode.has("status_message")){
            return new ResponseStatusException(
                    HttpStatus.valueOf(jsonNode.get("status_code").intValue()), jsonNode.get("status_message").textValue());
        }

        // Check if the error comes from the Breeze API or WeatherBit API
        JsonNode error = jsonNode.get("error");
        if (error.has("detail")) {
            errorMessage = error.get("detail").textValue();
        } else {
            errorMessage = error.textValue();
        }

        return new ResponseStatusException(
                HttpStatus.valueOf(e.getRawStatusCode()), errorMessage);
    }

    // Method used to throw an appropriate error when a bad argument is given
    public ResponseStatusException handleIllegalArgumentException(IllegalStateException e) {
        throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, e.getMessage());
    }

    public ResponseStatusException handleNoLocaleException() {
        return new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Error! Couldn't find any locale with the specified name");
    }

    public ResponseStatusException handleUnexpectedException() {
        return new ResponseStatusException(
                HttpStatus.INTERNAL_SERVER_ERROR, "An unexpected error has occurred. This was most likely caused by our external APIs. Please try again in 5-10 seconds or contact us!");
    }


    private ResponseEntity<AirResponse> qualityByCoord(double lat, double lon, String api, String reqType) {
        try {
            AirResponse response = aetherService.getQualityByCoord(lat, lon, reqType, api, null);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (RestClientResponseException e) {
            throw this.handleAPIException(e);
        } catch (IllegalStateException e) {
            throw this.handleIllegalArgumentException(e);
        } catch (Exception generic) {
            throw handleUnexpectedException();
        }
    }

    private ResponseEntity<AirResponse> qualityByLocale(String locale, String api, String reqType) {
        try {
            AirResponse response = aetherService.getQualityByLocale(locale, reqType, api);
            if (response == null) {
                throw new IllegalArgumentException();
            }

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (RestClientResponseException e) {
            throw this.handleAPIException(e);
        } catch (IllegalStateException e) {
            throw this.handleIllegalArgumentException(e);
        } catch (IllegalArgumentException e) {
            throw this.handleNoLocaleException();
        } catch (Exception generic) {
            throw handleUnexpectedException();
        }
    }
}
