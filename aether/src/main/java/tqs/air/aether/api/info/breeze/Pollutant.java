package tqs.air.aether.api.info.breeze;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@EqualsAndHashCode
@Setter
@NoArgsConstructor
public class Pollutant {
    private String name;
    private double concentration;
    private String unit;

    private int aqi;


}
