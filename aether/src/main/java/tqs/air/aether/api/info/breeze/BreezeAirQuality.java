package tqs.air.aether.api.info.breeze;

import lombok.EqualsAndHashCode;
import tqs.air.aether.api.info.AirQualityItem;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
public class BreezeAirQuality implements AirQualityItem {
    private int baqi;
    private Map<String, Pollutant> pollutants;
    private String date;

    public BreezeAirQuality() {
        this.pollutants = new HashMap<>();
    }

    public void addPollutant(String polAcronym, Pollutant newPol) {
        this.pollutants.put(polAcronym, newPol);
    }
}

