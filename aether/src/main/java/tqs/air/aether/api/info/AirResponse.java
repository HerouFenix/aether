package tqs.air.aether.api.info;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@EqualsAndHashCode
public class AirResponse {
    private String apiName;
    private Coordinates coordinates;
    private LocalDateTime requestDate;
    private Map<Integer, AirQualityItem> data;

    public AirResponse() {
        this.data = new HashMap<>();
    }

    public void addToData(AirQualityItem item, int key) {
        this.data.put(key, item);
    }
}
