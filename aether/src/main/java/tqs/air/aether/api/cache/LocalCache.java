package tqs.air.aether.api.cache;

import lombok.Getter;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import tqs.air.aether.api.info.AirResponse;
import tqs.air.aether.api.info.Coordinates;


import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

@Configuration
@EnableScheduling
@Component
@Getter
public class LocalCache {
    // Note, originally I wanted to have these variables be retreived from the applications.properties but this could not be done
    // for static final constants.
    private static final int TIMEOUT = 60000; // 1 minute

    private static final int MAX_CACHE_SIZE = 150;

    private Map<CacheKey, AirResponse> cache;
    private Queue<CacheKey> cacheQueue;

    private int size;

    private int hits;
    private int misses;
    private int totalRequests;


    public LocalCache() {
        this.cache = new HashMap<>();
        this.cacheQueue = new LinkedList<>();

        this.hits = 0;
        this.misses = 0;
        this.totalRequests = 0;

        this.size = 0;
    }

    public int getTimeout() {
        return TIMEOUT;
    }

    public int getMaxCacheSize() {
        return MAX_CACHE_SIZE;
    }

    public AirResponse getFromCache(Coordinates coordinates, @Nullable String api, String reqTime) {
        CacheKey key;
        AirResponse returnVal;

        // If no api is specified try to get it from Breeze, and if that doesn't work, from Weather
        if (api != null) {
            key = new CacheKey(coordinates, api, reqTime);
            returnVal = this.cache.get(key);
        } else {
            key = new CacheKey(coordinates, "Breeze", reqTime);
            returnVal = this.cache.get(key);

            if (returnVal == null) {
                key = new CacheKey(coordinates, "Weather", reqTime);
                returnVal = this.cache.get(key);
            }
        }

        this.totalRequests += 1;

        //Increase Hits if value is in Cache, Misses if not
        if (returnVal != null) {
            // Make sure that its not stale date
            if (Duration.between(returnVal.getRequestDate(), LocalDateTime.now()).toMillis() > TIMEOUT) {
                this.cache.remove(key);
                this.cacheQueue.poll();
                this.size -= 1;

                this.misses += 1;
                return null;
            }

            this.hits += 1;
        } else {
            this.misses += 1;
        }

        return returnVal;
    }

    public void addToCache(AirResponse newItem, String reqTime) {
        CacheKey newKey = new CacheKey(newItem.getCoordinates(), newItem.getApiName(), reqTime);

        // If our queue is full, remove the oldest element
        if (this.cacheQueue.size() >= MAX_CACHE_SIZE) {
            CacheKey oldestKey = this.cacheQueue.poll();
            this.cache.remove(oldestKey);

            this.size -= 1;
        }

        // If we already contain this item, update it and send it to the end of the queue, else create it
        this.cache.put(newKey, newItem);
        this.cacheQueue.add(newKey);

        this.size += 1;
    }

    public void resetCache() {
        this.cache = new HashMap<>();
        this.cacheQueue = new LinkedList<>();

        this.hits = 0;
        this.misses = 0;
        this.totalRequests = 0;

        this.size = 0;
    }

    @Scheduled(fixedDelay = TIMEOUT)
    public void removeFromCache() {
        if (this.size > 0) {
            CacheKey oldestKey = this.cacheQueue.element();

            AirResponse elementToRemove = this.cache.get(oldestKey);
            if (Duration.between(elementToRemove.getRequestDate(), LocalDateTime.now()).toMillis() > TIMEOUT) {
                this.cache.remove(oldestKey);
                this.cacheQueue.poll();
                this.size -= 1;
            }
        }
    }
}
