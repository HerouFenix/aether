package tqs.air.aether.api.info;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import tqs.air.aether.api.info.breeze.BreezeAirQuality;
import tqs.air.aether.api.info.weatherbit.WeatherAirQuality;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "apiName")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BreezeAirQuality.class, name = "Breeze"),
        @JsonSubTypes.Type(value = WeatherAirQuality.class, name = "Weather"),
})
public interface AirQualityItem {
}
