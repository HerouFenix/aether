package tqs.air.aether.api;

import org.springframework.web.client.RestClientResponseException;
import tqs.air.aether.api.cache.CacheKey;
import tqs.air.aether.api.cache.LocalCache;
import tqs.air.aether.api.info.AirQualityItem;
import tqs.air.aether.api.info.AirResponse;
import tqs.air.aether.api.info.Coordinates;
import tqs.air.aether.api.info.breeze.BreezeAirQuality;
import tqs.air.aether.api.info.weatherbit.WeatherAirQuality;
import tqs.air.aether.api.locale.Locale;
import tqs.air.aether.api.locale.LocalePOJO;
import tqs.air.aether.api.locale.LocaleRepository;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.junit.jupiter.MockitoExtension;

import org.springframework.web.client.HttpClientErrorException;

import java.time.LocalDateTime;
import java.util.*;


@ExtendWith(MockitoExtension.class)
class AetherServiceUT {
    @Mock(lenient = true)
    private LocaleRepository localeRepository;

    @Mock(lenient = true)
    private LocalCache localCache;

    @InjectMocks
    private AetherService aetherService = new AetherService("https://api.breezometer.com/air-quality/v2", "139e4f85555a480b86f1f9ef79043f1c", "https://api.weatherbit.io/v2.0", "bbef17dca94340a1b339b7d75ae0ec62");

    @InjectMocks
    private AetherService aetherServiceInvalidKeys = new AetherService("https://api.breezometer.com/air-quality/v2", "0", "https://api.weatherbit.io/v2.0", "0");

    private void setUpRepo() {
        Locale aveiro = new Locale("Aveiro", 8.6538, 40.6405);
        Locale porto = new Locale("Porto", 8.6291, 41.1579);
        Locale lisboa = new Locale("Lisboa", 9.1393, 38.7223);

        List<Locale> allLocales = Arrays.asList(aveiro, porto, lisboa);

        Mockito.when(localeRepository.findByName(aveiro.getName())).thenReturn(aveiro);
        Mockito.when(localeRepository.findByName(porto.getName())).thenReturn(porto);
        Mockito.when(localeRepository.findByName(lisboa.getName())).thenReturn(lisboa);

        Mockito.when(localeRepository.findByName("NON EXISTANT")).thenReturn(null);

        Mockito.when(localeRepository.findAll()).thenReturn(allLocales);
    }

    private void setUpCache() {
        AirResponse espinhoQuality = new AirResponse();
        Coordinates espinhoCoord = new Coordinates(8.6410, 41.0072);
        CacheKey espinhoKey = new CacheKey(espinhoCoord, "Breeze", "CURRENT");
        Map<Integer, AirQualityItem> espinhoData = new HashMap<>();
        BreezeAirQuality testEspinho = new BreezeAirQuality();
        testEspinho.setBaqi(69);
        espinhoData.put(0, testEspinho);

        espinhoQuality.setApiName("Breeze");
        espinhoQuality.setCoordinates(espinhoCoord);
        espinhoQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 0, 0, 0));
        espinhoQuality.setData(espinhoData);

        AirResponse lisboaQuality = new AirResponse();
        Coordinates lisboaCoord = new Coordinates(9.1393, 38.7223);
        CacheKey lisboaKey = new CacheKey(lisboaCoord, "Weather", "CURRENT");
        Map<Integer, AirQualityItem> lisboaData = new HashMap<>();
        WeatherAirQuality testLisboa = new WeatherAirQuality();
        testLisboa.setAqi(5);
        lisboaData.put(0, testLisboa);
        testLisboa.setAqi(10);
        lisboaData.put(1, testLisboa);

        lisboaQuality.setApiName("Weather");
        lisboaCoord.setLocale("Lisboa");
        lisboaQuality.setCoordinates(lisboaCoord);
        lisboaQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 1, 0, 0));
        lisboaQuality.setData(lisboaData);

        lisboaQuality.setApiName("Weather");
        lisboaCoord.setLocale("Lisboa");
        lisboaQuality.setCoordinates(lisboaCoord);
        lisboaQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 1, 0, 0));
        lisboaQuality.setData(lisboaData);

        AirResponse ovarQuality = new AirResponse();
        Coordinates ovarCoord = new Coordinates(8.6253, 40.8596);
        CacheKey ovarKey = new CacheKey(ovarCoord, "Breeze", "PAST");
        Map<Integer, AirQualityItem> ovarData = new HashMap<>();
        WeatherAirQuality testOvar = new WeatherAirQuality();
        testOvar.setAqi(15);
        ovarData.put(0, testOvar);
        testOvar.setAqi(18);
        ovarData.put(1, testOvar);

        ovarQuality.setApiName("Breeze");
        ovarQuality.setCoordinates(ovarCoord);
        ovarQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 21, 1, 0, 0));
        ovarQuality.setData(ovarData);

        AirResponse aveiroQuality = new AirResponse();
        Coordinates aveiroCoord = new Coordinates(8.6538, 40.6405);
        CacheKey aveiroKey = new CacheKey(aveiroCoord, "Weather", "FUTURE");
        Map<Integer, AirQualityItem> aveiroData = new HashMap<>();
        WeatherAirQuality testAveiro = new WeatherAirQuality();
        testAveiro.setAqi(15);
        aveiroData.put(0, testAveiro);
        testAveiro.setAqi(2);
        aveiroData.put(1, testAveiro);

        aveiroQuality.setApiName("Weather");
        aveiroCoord.setLocale("Aveiro");
        aveiroQuality.setCoordinates(aveiroCoord);
        aveiroQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 1, 42, 0));
        aveiroQuality.setData(aveiroData);


        Map<CacheKey, AirResponse> cache = new HashMap<>();
        cache.put(aveiroKey, aveiroQuality);
        cache.put(espinhoKey, espinhoQuality);
        cache.put(lisboaKey, lisboaQuality);
        cache.put(ovarKey, ovarQuality);

        Mockito.when(localCache.getSize()).thenReturn(4);

        Mockito.when(localCache.getHits()).thenReturn(5);
        Mockito.when(localCache.getMisses()).thenReturn(4);
        Mockito.when(localCache.getTotalRequests()).thenReturn(9);

        Mockito.when(localCache.getFromCache(aveiroCoord, "Weather", "FUTURE")).thenReturn(aveiroQuality);

        Mockito.when(localCache.getFromCache(espinhoCoord, "Breeze", "CURRENT")).thenReturn(espinhoQuality);

        Mockito.when(localCache.getFromCache(lisboaCoord, "Weather", "CURRENT")).thenReturn(lisboaQuality);

        Mockito.when(localCache.getFromCache(ovarCoord, "Breeze", "PAST")).thenReturn(ovarQuality);

        Mockito.when(localCache.getFromCache(aveiroCoord, null, "FUTURE")).thenReturn(aveiroQuality);

        Mockito.when(localCache.getFromCache(espinhoCoord, null, "CURRENT")).thenReturn(espinhoQuality);

        Mockito.when(localCache.getFromCache(lisboaCoord, null, "CURRENT")).thenReturn(lisboaQuality);

        Mockito.when(localCache.getFromCache(ovarCoord, null, "PAST")).thenReturn(ovarQuality);


        Mockito.when(localCache.getCache()).thenReturn(cache);
    }

    @BeforeEach
    public void setUp()
    {
        Mockito.reset(localCache);
        Mockito.reset(localeRepository);

        // Setup Locale Repo
        setUpRepo();

        // Setup LocalCache
        setUpCache();
    }
    
    @Test
    public void whenCreatingNonExistingLocale_thenReturnLocale() {
        LocalePOJO espinhoPOJO = new LocalePOJO("Espinho", 8.6410, 41.0072);
        Locale espinhoLocale = new Locale("Espinho", 8.6410, 41.0072);

        // Note: Due to the fact that the service method creates a new Locale (since, as recommended by SonarQube, it receives a POJO instead of a Locale entity), and because the ID is auto generated
        // I had to generalize the save method.
        Mockito.when(localeRepository.save(Mockito.any(Locale.class))).thenReturn(espinhoLocale);

        Locale created = aetherService.createLocale(espinhoPOJO);

        Mockito.verify(localeRepository, VerificationModeFactory.times(1)).save(Mockito.any(Locale.class));
        Mockito.reset(localeRepository);

        assertThat(created.getName()).isEqualTo(espinhoPOJO.getName());
        assertThat(created.getLon()).isEqualTo(espinhoPOJO.getLon());
        assertThat(created.getLat()).isEqualTo(espinhoPOJO.getLat());
    }

    @Test
    public void whenCreatingExistingLocale_thenReturnNull() {
        LocalePOJO aveiroPOJO = new LocalePOJO("Aveiro", 8.6538, 40.6405);

        Locale created = aetherService.createLocale(aveiroPOJO);

        Mockito.verify(localeRepository, VerificationModeFactory.times(0)).save(Mockito.any(Locale.class));
        Mockito.reset(localeRepository);

        assertThat(created).isNull();
    }

    @Test
    public void whenUpdatingExistingLocale_thenReturnUpdatedLocale() {
        LocalePOJO aveiroPOJO = new LocalePOJO("Aveiro", 8.8, 41);
        Locale aveiro = new Locale("Aveiro", 8.8, 41);

        Mockito.when(localeRepository.save(Mockito.any(Locale.class))).thenReturn(aveiro);

        Locale updated = aetherService.updateLocale(aveiroPOJO);

        Mockito.verify(localeRepository, VerificationModeFactory.times(1)).save(Mockito.any(Locale.class));
        Mockito.reset(localeRepository);

        assertThat(updated.getName()).isEqualTo(aveiroPOJO.getName());
        assertThat(updated.getLon()).isEqualTo(aveiroPOJO.getLon());
        assertThat(updated.getLat()).isEqualTo(aveiroPOJO.getLat());
    }

    @Test
    public void whenUpdatingNonExistingLocale_thenReturnNull() {
        LocalePOJO espinhoPOJO = new LocalePOJO("Espinho", 8.6410, 41.0072);

        Locale updated = aetherService.updateLocale(espinhoPOJO);

        Mockito.verify(localeRepository, VerificationModeFactory.times(0)).save(Mockito.any(Locale.class));
        Mockito.reset(localeRepository);

        assertThat(updated).isNull();
    }

    @Test
    public void whenDeletingExistingLocale_thenReturnDeletedLocale() {
        Locale aveiro = new Locale("Aveiro", 8.6538, 40.6405);

        Locale deleted = aetherService.deleteLocale(aveiro.getName());

        assertThat(deleted.getName()).isEqualTo(aveiro.getName());
        assertThat(deleted.getLon()).isEqualTo(aveiro.getLon());
        assertThat(deleted.getLat()).isEqualTo(aveiro.getLat());

        Locale lisboa = new Locale("Lisboa", 9.1393, 38.7223);

        deleted = aetherService.deleteLocale(lisboa.getName());

        assertThat(deleted.getName()).isEqualTo(lisboa.getName());
        assertThat(deleted.getLon()).isEqualTo(lisboa.getLon());
        assertThat(deleted.getLat()).isEqualTo(lisboa.getLat());

        Mockito.verify(localeRepository, VerificationModeFactory.times(2)).delete(Mockito.any(Locale.class));
        Mockito.reset(localeRepository);
    }

    @Test
    public void whenDeletingNonExistLocale_thenReturnNull() {
        Locale espinho = new Locale("Espinho", 8.6410, 41.0072);

        Locale deleted = aetherService.deleteLocale(espinho.getName());

        Mockito.verify(localeRepository, VerificationModeFactory.times(0)).delete(Mockito.any(Locale.class));
        Mockito.reset(localeRepository);

        assertThat(deleted).isNull();
    }

    @Test
    public void whenGettingAllLocales_thenReturnArrayOfLocales() {
        Locale aveiro = new Locale("Aveiro", 8.6538, 40.6405);
        Locale porto = new Locale("Porto", 8.6291, 41.1579);
        Locale lisboa = new Locale("Lisboa", 9.1393, 38.7223);

        List<Locale> allLocales = aetherService.getAllLocales();

        Mockito.verify(localeRepository, VerificationModeFactory.times(1)).findAll();
        Mockito.reset(localeRepository);

        assertThat(allLocales).hasSize(3).extracting(Locale::getName).contains(aveiro.getName(), porto.getName(), lisboa.getName());
        assertThat(allLocales).extracting(Locale::getLat).contains(aveiro.getLat(), porto.getLat(), lisboa.getLat());
        assertThat(allLocales).extracting(Locale::getLon).contains(aveiro.getLon(), porto.getLon(), lisboa.getLon());
    }

    @Test
    public void whenGettingCache_thenReturnArrayOfResponses() {
        AirResponse espinhoQuality = new AirResponse();
        Coordinates espinhoCoord = new Coordinates(8.6410, 41.0072);
        CacheKey espinhoKey = new CacheKey(espinhoCoord, "Breeze", "CURRENT");
        Map<Integer, AirQualityItem> espinhoData = new HashMap<>();
        BreezeAirQuality testEspinho = new BreezeAirQuality();
        testEspinho.setBaqi(69);
        espinhoData.put(0, testEspinho);

        espinhoQuality.setApiName("Breeze");
        espinhoQuality.setCoordinates(espinhoCoord);
        espinhoQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 0, 0, 0));
        espinhoQuality.setData(espinhoData);

        AirResponse lisboaQuality = new AirResponse();
        Coordinates lisboaCoord = new Coordinates(9.1393, 38.7223);
        CacheKey lisboaKey = new CacheKey(lisboaCoord, "Weather", "CURRENT");
        Map<Integer, AirQualityItem> lisboaData = new HashMap<>();
        WeatherAirQuality testLisboa = new WeatherAirQuality();
        testLisboa.setAqi(5);
        lisboaData.put(0, testLisboa);
        testLisboa.setAqi(10);
        lisboaData.put(1, testLisboa);

        lisboaQuality.setApiName("Weather");
        lisboaCoord.setLocale("Lisboa");
        lisboaQuality.setCoordinates(lisboaCoord);
        lisboaQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 1, 0, 0));
        lisboaQuality.setData(lisboaData);


        AirResponse aveiroQuality = new AirResponse();
        Coordinates aveiroCoord = new Coordinates(8.6538, 40.6405);
        Map<Integer, AirQualityItem> aveiroData = new HashMap<>();
        WeatherAirQuality testAveiro = new WeatherAirQuality();
        testAveiro.setAqi(15);
        aveiroData.put(0, testAveiro);
        testAveiro.setAqi(2);
        aveiroData.put(1, testAveiro);

        aveiroQuality.setApiName("Weather");
        aveiroCoord.setLocale("Aveiro");
        aveiroQuality.setCoordinates(aveiroCoord);
        aveiroQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 1, 42, 0));
        aveiroQuality.setData(aveiroData);

        AirResponse ovarQuality = new AirResponse();
        Coordinates ovarCoord = new Coordinates(8.6253, 40.8596);
        Map<Integer, AirQualityItem> ovarData = new HashMap<>();
        WeatherAirQuality testOvar = new WeatherAirQuality();
        testOvar.setAqi(15);
        ovarData.put(0, testOvar);
        testOvar.setAqi(18);
        ovarData.put(1, testOvar);

        ovarQuality.setApiName("Breeze");
        ovarQuality.setCoordinates(ovarCoord);
        ovarQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 21, 1, 0, 0));
        ovarQuality.setData(ovarData);


        List<AirResponse> allLocales = aetherService.getEntireCache();

        Mockito.verify(localCache, VerificationModeFactory.times(1)).getCache();
        Mockito.reset(localCache);

        assertThat(allLocales).hasSize(4).contains(espinhoQuality, lisboaQuality, ovarQuality, aveiroQuality);
    }

    @Test
    public void whenGettingCacheSize_thenReturnCacheSize() {
        int size = aetherService.getCacheSize();
        int expectedSize = 4;

        Mockito.verify(localCache, VerificationModeFactory.times(1)).getSize();
        Mockito.reset(localCache);

        assertThat(size).isEqualTo(expectedSize);
    }

    @Test
    public void whenGettingCacheHitsNMisses_thenReturnMapWithHitsNMisses() {
        Map<String, Integer> hitsnmisses = aetherService.getHitsNMisses();
        int expectedHits = 5;
        int expectedMisses = 4;
        int expectedTotalRequests = 9;

        Mockito.verify(localCache, VerificationModeFactory.times(1)).getHits();
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getMisses();
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getTotalRequests();

        Mockito.reset(localCache);

        assertThat(hitsnmisses).hasSize(3);
        assertThat(hitsnmisses.get("Hits")).isEqualTo(expectedHits);
        assertThat(hitsnmisses.get("Misses")).isEqualTo(expectedMisses);
        assertThat(hitsnmisses.get("TotalRequests")).isEqualTo(expectedTotalRequests);
    }

    @Test
    public void whenGettingQualityByLocale_andLocaleInvalid_returnNull() throws JsonProcessingException {
        Locale espinho = new Locale("Espinho", 8.6410, 41.0072);

        // Note, since the locale verification is done before either the API or Request Type parameter are used, there's no need to test for all of them
        AirResponse response = aetherService.getQualityByLocale(espinho.getName(), "CURRENT", null);

        assertThat(response).isNull();

        Mockito.verify(localeRepository, VerificationModeFactory.times(1)).findByName(espinho.getName());
        Mockito.reset(localeRepository);
    }

    @Test
    public void whenGettingQualityByLocale_andGivenAPIInvalid_throwException() throws JsonProcessingException {
        Locale aveiro = new Locale("Aveiro", 8.6538, 40.6405);

        Exception exception = assertThrows(IllegalStateException.class, () -> {
            aetherService.getQualityByLocale(aveiro.getName(), "PAST", "InvalidAPI");
        });
        assertThat(exception.getMessage()).isEqualTo("Unexpected API value : InvalidAPI . Was expecting 'Breeze', 'Weather' or nothing");

        exception = assertThrows(IllegalStateException.class, () -> {
            aetherService.getQualityByLocale(aveiro.getName(), "CURRENT", "InvalidAPI");
        });
        assertThat(exception.getMessage()).isEqualTo("Unexpected API value : InvalidAPI . Was expecting 'Breeze', 'Weather' or nothing");

        exception = assertThrows(IllegalStateException.class, () -> {
            aetherService.getQualityByLocale(aveiro.getName(), "FUTURE", "InvalidAPI");
        });
        assertThat(exception.getMessage()).isEqualTo("Unexpected API value : InvalidAPI . Was expecting 'Breeze', 'Weather' or nothing");

        Mockito.verify(localeRepository, VerificationModeFactory.times(3)).findByName(aveiro.getName());
        Mockito.reset(localeRepository);
    }

    @Test
    public void whenGettingQualityByLocale_andLocaleValid_returnLocaleIsDefined() throws JsonProcessingException {
        Locale aveiro = new Locale("Aveiro", 8.6538, 40.6405);

        AirResponse resultCurrent = aetherService.getQualityByLocale(aveiro.getName(), "CURRENT", null);

        assertThat(resultCurrent.getCoordinates().getLocale()).isEqualTo(aveiro.getName());
    }

    @Test
    public void whenGettingQualityByCoordBreeze_AndNotInCache_returnInfo_andAddToCache() throws JsonProcessingException {
        double lat = 8.4043;
        double lon = 39.6054;
        Coordinates coords = new Coordinates(lat, lon);

        String api = "Breeze";

        // PAST
        AirResponse resultCurrent = aetherService.getQualityByCoord(lat, lon, "PAST", api, null);

        // Verify we added to the cache at the end
        Mockito.verify(localCache, VerificationModeFactory.times(1)).addToCache(resultCurrent, "PAST");

        assertThat(resultCurrent.getCoordinates().getLat()).isEqualTo(lat);
        assertThat(resultCurrent.getCoordinates().getLon()).isEqualTo(lon);
        assertThat(resultCurrent.getData().size()).isEqualTo(3);

        // CURRENT
        resultCurrent = aetherService.getQualityByCoord(lat, lon, "CURRENT", api, null);

        // Verify we added to the cache at the end
        Mockito.verify(localCache, VerificationModeFactory.times(1)).addToCache(resultCurrent, "CURRENT");

        assertThat(resultCurrent.getCoordinates().getLat()).isEqualTo(lat);
        assertThat(resultCurrent.getCoordinates().getLon()).isEqualTo(lon);
        assertThat(resultCurrent.getData().size()).isEqualTo(1);

        // FUTURE
        resultCurrent = aetherService.getQualityByCoord(lat, lon, "FUTURE", api, null);

        // Verify we added to the cache at the end
        Mockito.verify(localCache, VerificationModeFactory.times(1)).addToCache(resultCurrent, "FUTURE");

        assertThat(resultCurrent.getCoordinates().getLat()).isEqualTo(lat);
        assertThat(resultCurrent.getCoordinates().getLon()).isEqualTo(lon);
        assertThat(resultCurrent.getData().size()).isEqualTo(3);

        // Verify we tried to get from the cache
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(coords, api, "CURRENT");
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(coords, api, "PAST");
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(coords, api, "FUTURE");

        Mockito.reset(localCache);
    }

    @Test
    public void whenGettingQualityByCoordWeather_AndNotInCache_returnInfo_andAddToCache() throws JsonProcessingException {
        double lat = 8.4043;
        double lon = 39.6054;
        Coordinates coords = new Coordinates(lat, lon);

        String api = "Weather";

        // PAST
        AirResponse resultCurrent = aetherService.getQualityByCoord(lat, lon, "PAST", api, null);

        // Verify we added to the cache at the end
        Mockito.verify(localCache, VerificationModeFactory.times(1)).addToCache(resultCurrent, "PAST");

        assertThat(resultCurrent.getCoordinates().getLat()).isEqualTo(lat);
        assertThat(resultCurrent.getCoordinates().getLon()).isEqualTo(lon);
        assertThat(resultCurrent.getData().size()).isGreaterThanOrEqualTo(72);

        // CURRENT
        resultCurrent = aetherService.getQualityByCoord(lat, lon, "CURRENT", api, null);

        // Verify we added to the cache at the end
        Mockito.verify(localCache, VerificationModeFactory.times(1)).addToCache(resultCurrent, "CURRENT");

        assertThat(resultCurrent.getCoordinates().getLat()).isEqualTo(lat);
        assertThat(resultCurrent.getCoordinates().getLon()).isEqualTo(lon);
        assertThat(resultCurrent.getData().size()).isEqualTo(1);

        // FUTURE
        resultCurrent = aetherService.getQualityByCoord(lat, lon, "FUTURE", api, null);

        // Verify we added to the cache at the end
        Mockito.verify(localCache, VerificationModeFactory.times(1)).addToCache(resultCurrent, "FUTURE");

        assertThat(resultCurrent.getCoordinates().getLat()).isEqualTo(lat);
        assertThat(resultCurrent.getCoordinates().getLon()).isEqualTo(lon);
        assertThat(resultCurrent.getData().size()).isGreaterThanOrEqualTo(72);

        // Verify we tried to get from the cache
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(coords, api, "CURRENT");
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(coords, api, "PAST");
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(coords, api, "FUTURE");

        Mockito.reset(localCache);
    }

    @Test
    public void whenGettingQualityByCoordNoAPI_AndNotInCache_returnInfo_andAddToCache() throws JsonProcessingException {
        double lat = 8.4043;
        double lon = 39.6054;
        Coordinates nonExistantCoords = new Coordinates(lat, lon);  // Coordinates of an item thats not in the Cache
        Coordinates aveiroCoord = new Coordinates(8.6538, 40.6405); // Is in the cache under FUTURE / Weather
        Coordinates espinhoCoord = new Coordinates(8.6410, 41.0072); // Is in the cache under CURRENT / Breeze
        Coordinates ovarCoord = new Coordinates(8.6253, 40.8596); // Is in the cache under PAST / Breeze

        String api = null;

        // PAST
        // Non Existant
        AirResponse resultCurrent = aetherService.getQualityByCoord(nonExistantCoords.getLat(), nonExistantCoords.getLon(), "PAST", api, null);

        // Verify we added to the cache at the end
        Mockito.verify(localCache, VerificationModeFactory.times(1)).addToCache(resultCurrent, "PAST");

        assertThat(resultCurrent.getCoordinates().getLat()).isEqualTo(nonExistantCoords.getLat());
        assertThat(resultCurrent.getCoordinates().getLon()).isEqualTo(nonExistantCoords.getLon());
        assertThat(resultCurrent.getData().size()).isGreaterThanOrEqualTo(3);

        // Espinho
        resultCurrent = aetherService.getQualityByCoord(espinhoCoord.getLat(), espinhoCoord.getLon(), "PAST", api, null);

        // Verify we added to the cache at the end
        Mockito.verify(localCache, VerificationModeFactory.times(1)).addToCache(resultCurrent, "PAST");


        // CURRENT
        resultCurrent = aetherService.getQualityByCoord(lat, lon, "CURRENT", api, null);

        // Verify we added to the cache at the end
        Mockito.verify(localCache, VerificationModeFactory.times(1)).addToCache(resultCurrent, "CURRENT");

        assertThat(resultCurrent.getCoordinates().getLat()).isEqualTo(lat);
        assertThat(resultCurrent.getCoordinates().getLon()).isEqualTo(lon);
        assertThat(resultCurrent.getData().size()).isEqualTo(1);

        // Aveiro
        resultCurrent = aetherService.getQualityByCoord(aveiroCoord.getLat(), aveiroCoord.getLon(), "CURRENT", api, null);

        // Verify we added to the cache at the end
        Mockito.verify(localCache, VerificationModeFactory.times(1)).addToCache(resultCurrent, "CURRENT");

        // FUTURE
        resultCurrent = aetherService.getQualityByCoord(lat, lon, "FUTURE", api, null);

        // Verify we added to the cache at the end
        Mockito.verify(localCache, VerificationModeFactory.times(1)).addToCache(resultCurrent, "FUTURE");


        assertThat(resultCurrent.getCoordinates().getLat()).isEqualTo(lat);
        assertThat(resultCurrent.getCoordinates().getLon()).isEqualTo(lon);
        assertThat(resultCurrent.getData().size()).isGreaterThanOrEqualTo(3);

        // Ovar
        resultCurrent = aetherService.getQualityByCoord(ovarCoord.getLat(), ovarCoord.getLon(), "FUTURE", api, null);

        // Verify we added to the cache at the end
        Mockito.verify(localCache, VerificationModeFactory.times(1)).addToCache(resultCurrent, "FUTURE");

        // Verify we tried to get from the cache
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(nonExistantCoords, api, "CURRENT");
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(nonExistantCoords, api, "PAST");
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(nonExistantCoords, api, "FUTURE");

        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(aveiroCoord, api, "CURRENT");
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(espinhoCoord, api, "PAST");
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(ovarCoord, api, "FUTURE");

        Mockito.reset(localCache);
    }

    @Test
    public void whenGettingQualityByCoordBreeze_AndInCache_returnInfo_andDontAddToCache() throws JsonProcessingException {
        String api = "Breeze";

        // During setup we assured that these items are in the cache
        AirResponse espinhoQuality = new AirResponse();
        Coordinates espinhoCoord = new Coordinates(8.6410, 41.0072);
        CacheKey espinhoKey = new CacheKey(espinhoCoord, "Breeze", "CURRENT");
        Map<Integer, AirQualityItem> espinhoData = new HashMap<>();
        BreezeAirQuality testEspinho = new BreezeAirQuality();
        testEspinho.setBaqi(69);
        espinhoData.put(0, testEspinho);

        espinhoQuality.setApiName("Breeze");
        espinhoQuality.setCoordinates(espinhoCoord);
        espinhoQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 0, 0, 0));
        espinhoQuality.setData(espinhoData);

        AirResponse ovarQuality = new AirResponse();
        Coordinates ovarCoord = new Coordinates(8.6253, 40.8596);
        CacheKey ovarKey = new CacheKey(ovarCoord, "Breeze", "PAST");
        Map<Integer, AirQualityItem> ovarData = new HashMap<>();
        WeatherAirQuality testOvar = new WeatherAirQuality();
        testOvar.setAqi(15);
        ovarData.put(0, testOvar);
        testOvar.setAqi(18);
        ovarData.put(1, testOvar);

        ovarQuality.setApiName("Breeze");
        ovarQuality.setCoordinates(ovarCoord);
        ovarQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 21, 1, 0, 0));
        ovarQuality.setData(ovarData);

        // CURRENT
        AirResponse resultCurrent = aetherService.getQualityByCoord(espinhoCoord.getLat(), espinhoCoord.getLon(), "CURRENT", api, null);

        assertThat(resultCurrent).isEqualTo(espinhoQuality);

        // Verify we tried to get from the cache
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(espinhoCoord, api, "CURRENT");


        // PAST
        resultCurrent = aetherService.getQualityByCoord(ovarCoord.getLat(), ovarCoord.getLon(), "PAST", api, null);

        assertThat(resultCurrent).isEqualTo(ovarQuality);

        // Verify we tried to get from the cache
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(ovarCoord, api, "PAST");


        // Verify we didn't add anything to the Cache
        Mockito.verify(localCache, VerificationModeFactory.times(0)).addToCache(Mockito.any(AirResponse.class), Mockito.anyString());

        Mockito.reset(localCache);
    }

    @Test
    public void whenGettingQualityByCoordWeather_AndInCache_returnInfo_andDontAddToCache() throws JsonProcessingException {
        String api = "Weather";

        // During setup we assured that these items are in the cache
        AirResponse lisboaQuality = new AirResponse();
        Coordinates lisboaCoord = new Coordinates(9.1393, 38.7223);
        Map<Integer, AirQualityItem> lisboaData = new HashMap<>();
        WeatherAirQuality testLisboa = new WeatherAirQuality();
        testLisboa.setAqi(5);
        lisboaData.put(0, testLisboa);
        testLisboa.setAqi(10);
        lisboaData.put(1, testLisboa);

        lisboaQuality.setApiName("Weather");
        lisboaCoord.setLocale("Lisboa");
        lisboaQuality.setCoordinates(lisboaCoord);
        lisboaQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 1, 0, 0));
        lisboaQuality.setData(lisboaData);


        AirResponse aveiroQuality = new AirResponse();
        Coordinates aveiroCoord = new Coordinates(8.6538, 40.6405);
        Map<Integer, AirQualityItem> aveiroData = new HashMap<>();
        WeatherAirQuality testAveiro = new WeatherAirQuality();
        testAveiro.setAqi(15);
        aveiroData.put(0, testAveiro);
        testAveiro.setAqi(2);
        aveiroData.put(1, testAveiro);

        aveiroQuality.setApiName("Weather");
        aveiroCoord.setLocale("Aveiro");
        aveiroQuality.setCoordinates(aveiroCoord);
        aveiroQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 1, 42, 0));
        aveiroQuality.setData(aveiroData);

        // CURRENT
        AirResponse resultCurrent = aetherService.getQualityByCoord(lisboaCoord.getLat(), lisboaCoord.getLon(), "CURRENT", api, null);

        assertThat(resultCurrent).isEqualTo(lisboaQuality);

        // Verify we tried to get from the cache
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(lisboaCoord, api, "CURRENT");


        // FUTURE
        resultCurrent = aetherService.getQualityByCoord(aveiroCoord.getLat(), aveiroCoord.getLon(), "FUTURE", api, null);

        assertThat(resultCurrent).isEqualTo(aveiroQuality);

        // Verify we tried to get from the cache
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(aveiroCoord, api, "FUTURE");


        // Verify we didn't add anything to the Cache
        Mockito.verify(localCache, VerificationModeFactory.times(0)).addToCache(Mockito.any(AirResponse.class), Mockito.anyString());

        Mockito.reset(localCache);
    }

    @Test
    public void whenGettingQualityByCoordNoAPI_AndInCache_returnInfo_andDontAddToCache() throws JsonProcessingException {
        String api = null;

        // During setup we assured that these items are in the cache
        AirResponse lisboaQuality = new AirResponse();
        Coordinates lisboaCoord = new Coordinates(9.1393, 38.7223);
        Map<Integer, AirQualityItem> lisboaData = new HashMap<>();
        WeatherAirQuality testLisboa = new WeatherAirQuality();
        testLisboa.setAqi(5);
        lisboaData.put(0, testLisboa);
        testLisboa.setAqi(10);
        lisboaData.put(1, testLisboa);

        lisboaQuality.setApiName("Weather");
        lisboaCoord.setLocale("Lisboa");
        lisboaQuality.setCoordinates(lisboaCoord);
        lisboaQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 1, 0, 0));
        lisboaQuality.setData(lisboaData);


        AirResponse aveiroQuality = new AirResponse();
        Coordinates aveiroCoord = new Coordinates(8.6538, 40.6405);
        Map<Integer, AirQualityItem> aveiroData = new HashMap<>();
        WeatherAirQuality testAveiro = new WeatherAirQuality();
        testAveiro.setAqi(15);
        aveiroData.put(0, testAveiro);
        testAveiro.setAqi(2);
        aveiroData.put(1, testAveiro);

        aveiroQuality.setApiName("Weather");
        aveiroCoord.setLocale("Aveiro");
        aveiroQuality.setCoordinates(aveiroCoord);
        aveiroQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 1, 42, 0));
        aveiroQuality.setData(aveiroData);

        AirResponse espinhoQuality = new AirResponse();
        Coordinates espinhoCoord = new Coordinates(8.6410, 41.0072);
        CacheKey espinhoKey = new CacheKey(espinhoCoord, "Breeze", "CURRENT");
        Map<Integer, AirQualityItem> espinhoData = new HashMap<>();
        BreezeAirQuality testEspinho = new BreezeAirQuality();
        testEspinho.setBaqi(69);
        espinhoData.put(0, testEspinho);

        espinhoQuality.setApiName("Breeze");
        espinhoQuality.setCoordinates(espinhoCoord);
        espinhoQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 0, 0, 0));
        espinhoQuality.setData(espinhoData);

        AirResponse ovarQuality = new AirResponse();
        Coordinates ovarCoord = new Coordinates(8.6253, 40.8596);
        CacheKey ovarKey = new CacheKey(ovarCoord, "Breeze", "PAST");
        Map<Integer, AirQualityItem> ovarData = new HashMap<>();
        WeatherAirQuality testOvar = new WeatherAirQuality();
        testOvar.setAqi(15);
        ovarData.put(0, testOvar);
        testOvar.setAqi(18);
        ovarData.put(1, testOvar);

        ovarQuality.setApiName("Breeze");
        ovarQuality.setCoordinates(ovarCoord);
        ovarQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 21, 1, 0, 0));
        ovarQuality.setData(ovarData);

        // CURRENT
        AirResponse resultCurrent = aetherService.getQualityByCoord(lisboaCoord.getLat(), lisboaCoord.getLon(), "CURRENT", api, null);

        assertThat(resultCurrent).isEqualTo(lisboaQuality);

        // Verify we tried to get from the cache
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(lisboaCoord, api, "CURRENT");

        resultCurrent = aetherService.getQualityByCoord(espinhoCoord.getLat(), espinhoCoord.getLon(), "CURRENT", api, null);

        assertThat(resultCurrent).isEqualTo(espinhoQuality);

        // Verify we tried to get from the cache
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(espinhoCoord, api, "CURRENT");


        // FUTURE
        resultCurrent = aetherService.getQualityByCoord(aveiroCoord.getLat(), aveiroCoord.getLon(), "FUTURE", api, null);

        assertThat(resultCurrent).isEqualTo(aveiroQuality);

        // Verify we tried to get from the cache
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(aveiroCoord, api, "FUTURE");

        // PAST
        resultCurrent = aetherService.getQualityByCoord(ovarCoord.getLat(), ovarCoord.getLon(), "PAST", api, null);

        assertThat(resultCurrent).isEqualTo(ovarQuality);

        // Verify we tried to get from the cache
        Mockito.verify(localCache, VerificationModeFactory.times(1)).getFromCache(ovarCoord, api, "PAST");

        // Verify we didn't add anything to the Cache
        Mockito.verify(localCache, VerificationModeFactory.times(0)).addToCache(Mockito.any(AirResponse.class), Mockito.anyString());

        Mockito.reset(localCache);
    }

    @Test
    public void whenGettingQualityByCoordBreeze_andInvalidCoord_throwException() {
        String api = "Breeze";
        Coordinates badCoords = new Coordinates(0, 0);  // Via testing the API we know these coordinates are not acceptable by BreezeOMeter

        Exception exception = assertThrows(HttpClientErrorException.class, () -> {
            aetherService.getQualityByCoord(badCoords.getLat(), badCoords.getLon(), "PAST", api, null);
        });
        assertThat(exception.getLocalizedMessage()).isEqualTo("400 Bad Request: [{\"metadata\": null, \"data\": null, \"error\": {\"code\": \"location_unsupported\", \"title\": \"Location Specified Is Unsupported\", \"detail\": \"The location specified in the request is unsupported. Make sure you've specified the 'lat' and 'lon' parameters within the coverage permissions set in your account. You can contact support@breezometer.com to get more information\"}}]");
    }

    @Test
    public void whenGettingQualityByCoordNoAPI_andBothAPIsError_throwException() {
        String api = null;
        Coordinates coords = new Coordinates(40, 2);

        RestClientResponseException exception = assertThrows(RestClientResponseException.class, () -> {
            aetherServiceInvalidKeys.getQualityByCoord(coords.getLat(), coords.getLon(), "CURRENT", api, null);
        });
        assertThat(exception.getResponseBodyAsString()).contains("{\"error\": \"Both APIs responded with errors - ");
        assertThat(exception.getResponseBodyAsString()).contains("Breeze: ");
        assertThat(exception.getResponseBodyAsString()).contains("WeatherBit: ");
    }

    @Test
    public void whenGettingQualityNoAPI_andInvalidCoordForBreeze_thenGetInfoFromWeather() throws JsonProcessingException {
        double lat = 0;
        double lon = 0;

        String api = null;

        // PAST
        AirResponse resultCurrent = aetherService.getQualityByCoord(lat, lon, "PAST", api, null);

        // Verify we added to the cache at the end
        Mockito.verify(localCache, VerificationModeFactory.times(1)).addToCache(resultCurrent, "PAST");

        assertThat(resultCurrent.getCoordinates().getLat()).isEqualTo(lat);
        assertThat(resultCurrent.getCoordinates().getLon()).isEqualTo(lon);
        assertThat(resultCurrent.getApiName()).isEqualTo("Weather");

        // CURRENT
        resultCurrent = aetherService.getQualityByCoord(lat, lon, "CURRENT", api, null);

        // Verify we added to the cache at the end
        Mockito.verify(localCache, VerificationModeFactory.times(1)).addToCache(resultCurrent, "CURRENT");

        assertThat(resultCurrent.getCoordinates().getLat()).isEqualTo(lat);
        assertThat(resultCurrent.getCoordinates().getLon()).isEqualTo(lon);
        assertThat(resultCurrent.getApiName()).isEqualTo("Weather");

        // FUTURE
        resultCurrent = aetherService.getQualityByCoord(lat, lon, "FUTURE", api, null);

        // Verify we added to the cache at the end
        Mockito.verify(localCache, VerificationModeFactory.times(1)).addToCache(resultCurrent, "FUTURE");

        assertThat(resultCurrent.getCoordinates().getLat()).isEqualTo(lat);
        assertThat(resultCurrent.getCoordinates().getLon()).isEqualTo(lon);
        assertThat(resultCurrent.getApiName()).isEqualTo("Weather");

        Mockito.reset(localCache);
    }
}
