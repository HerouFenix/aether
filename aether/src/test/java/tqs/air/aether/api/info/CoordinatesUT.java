package tqs.air.aether.api.info;

import org.junit.jupiter.api.Test;

import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class CoordinatesUT {

    @Test
    void testEquals() {
        Coordinates coordA = new Coordinates(5,5);
        Coordinates coordB = new Coordinates(10,-4);
        Coordinates coordC = new Coordinates(5,5);
        Coordinates coordD = null;
        Coordinates coordE = new Coordinates(5,-4);

        String notCoord = "(5, 5)";

        coordA.setLocale("Aveiro");
        coordB.setLocale("Aveiro");

        //Given these coordinates, coordA should be equal to coordC, since only the coordinates matter, and all other coords are different

        assertTrue(coordA.equals(coordC));
        assertTrue(coordA.equals(coordA));
        assertTrue(coordC.equals(coordA));
        assertTrue(coordC.equals(coordC));
        assertTrue(coordB.equals(coordB));
        assertTrue(coordE.equals(coordE));


        assertFalse(coordA.equals(coordB));
        assertFalse(coordA.equals(coordD));
        assertFalse(coordA.equals(notCoord));
        assertFalse(coordA.equals(coordE));

        assertFalse(coordC.equals(coordB));
        assertFalse(coordC.equals(coordD));
        assertFalse(coordC.equals(notCoord));
        assertFalse(coordC.equals(coordE));

        assertFalse(coordB.equals(coordA));
        assertFalse(coordB.equals(coordC));
        assertFalse(coordB.equals(coordD));
        assertFalse(coordB.equals(notCoord));
        assertFalse(coordB.equals(coordE));

        assertFalse(coordE.equals(coordA));
        assertFalse(coordE.equals(coordC));
        assertFalse(coordE.equals(coordD));
        assertFalse(coordE.equals(notCoord));
        assertFalse(coordE.equals(coordB));
    }

    @Test
    void testHashCode() {
        double aLat = 5;
        double aLon = 5;
        double bLat = 10;
        double bLon = -4;

        Coordinates coordA = new Coordinates(aLat,aLon);
        Coordinates coordB = new Coordinates(bLat,bLon);

        coordA.setLocale("Aveiro");

        assertThat(coordA.hashCode()).isEqualTo(Objects.hash(aLat, aLon));
        assertThat(coordB.hashCode()).isEqualTo(Objects.hash(bLat, bLon));
    }

    @Test
    void testToString() {
        double aLat = 5;
        double aLon = 5;
        double bLat = 10;
        double bLon = -4;
        String locale = "Aveiro";

        Coordinates coordA = new Coordinates(aLat,aLon);
        Coordinates coordB = new Coordinates(bLat,bLon);

        coordA.setLocale(locale);

        assertThat(coordA.toString()).isEqualTo("(lat=" + aLat + ", lon=" + aLon +")");
        assertThat(coordB.toString()).isEqualTo("(lat=" + bLat + ", lon=" + bLon +")");
    }
}