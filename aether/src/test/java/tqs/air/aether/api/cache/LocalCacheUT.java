package tqs.air.aether.api.cache;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tqs.air.aether.api.AetherService;
import tqs.air.aether.api.info.AirQualityItem;
import tqs.air.aether.api.info.AirResponse;
import tqs.air.aether.api.info.Coordinates;
import tqs.air.aether.api.info.breeze.BreezeAirQuality;
import tqs.air.aether.api.info.weatherbit.WeatherAirQuality;
import tqs.air.aether.api.locale.Locale;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class LocalCacheUT {
    private LocalCache localCache = new LocalCache();

    @BeforeEach
    public void setUp() {
        localCache.resetCache();

        AirResponse espinhoQuality = new AirResponse();
        Coordinates espinhoCoord = new Coordinates(8.6410, 41.0072);
        Map<Integer, AirQualityItem> espinhoData = new HashMap<>();
        BreezeAirQuality testEspinho = new BreezeAirQuality();
        testEspinho.setBaqi(69);
        espinhoData.put(0, testEspinho);

        espinhoQuality.setApiName("Breeze");
        espinhoQuality.setCoordinates(espinhoCoord);
        espinhoQuality.setRequestDate(LocalDateTime.now());
        espinhoQuality.setData(espinhoData);

        localCache.addToCache(espinhoQuality, "CURRENT");

        AirResponse lisboaQuality = new AirResponse();
        Coordinates lisboaCoord = new Coordinates(9.1393, 38.7223);
        Map<Integer, AirQualityItem> lisboaData = new HashMap<>();
        WeatherAirQuality testLisboa = new WeatherAirQuality();
        testLisboa.setAqi(5);
        lisboaData.put(0, testLisboa);
        testLisboa.setAqi(10);
        lisboaData.put(1, testLisboa);

        lisboaQuality.setApiName("Weather");
        lisboaCoord.setLocale("Lisboa");
        lisboaQuality.setCoordinates(lisboaCoord);
        lisboaQuality.setRequestDate(LocalDateTime.now());
        lisboaQuality.setData(lisboaData);

        lisboaQuality.setApiName("Weather");
        lisboaCoord.setLocale("Lisboa");
        lisboaQuality.setCoordinates(lisboaCoord);
        lisboaQuality.setRequestDate(LocalDateTime.now());
        lisboaQuality.setData(lisboaData);

        localCache.addToCache(lisboaQuality, "FUTURE");

        AirResponse ovarQuality = new AirResponse();
        Coordinates ovarCoord = new Coordinates(8.6253, 40.8596);
        Map<Integer, AirQualityItem> ovarData = new HashMap<>();
        WeatherAirQuality testOvar = new WeatherAirQuality();
        testOvar.setAqi(15);
        ovarData.put(0, testOvar);
        testOvar.setAqi(18);
        ovarData.put(1, testOvar);

        ovarQuality.setApiName("Breeze");
        ovarQuality.setCoordinates(ovarCoord);
        ovarQuality.setRequestDate(LocalDateTime.now());
        ovarQuality.setData(ovarData);

        localCache.addToCache(ovarQuality, "PAST");
    }

    @Test
    public void whenValidItem_andGivenAPI_thenReturnCorrectItem_andIncrementHitsAndRequests() {
        AirResponse espinhoQuality = new AirResponse();
        Coordinates espinhoCoord = new Coordinates(8.6410, 41.0072);

        Map<Integer, AirQualityItem> espinhoData = new HashMap<>();
        BreezeAirQuality testEspinho = new BreezeAirQuality();
        testEspinho.setBaqi(69);
        espinhoData.put(0, testEspinho);

        espinhoQuality.setApiName("Breeze");
        espinhoQuality.setCoordinates(espinhoCoord);
        espinhoQuality.setData(espinhoData);

        String apiEspinho = "Breeze";
        String reqTypeEspinho = "CURRENT";

        AirResponse lisboaQuality = new AirResponse();
        Coordinates lisboaCoord = new Coordinates(9.1393, 38.7223);
        Map<Integer, AirQualityItem> lisboaData = new HashMap<>();
        WeatherAirQuality testLisboa = new WeatherAirQuality();
        testLisboa.setAqi(5);
        lisboaData.put(0, testLisboa);
        testLisboa.setAqi(10);
        lisboaData.put(1, testLisboa);

        lisboaQuality.setApiName("Weather");
        lisboaCoord.setLocale("Lisboa");
        lisboaQuality.setCoordinates(lisboaCoord);
        lisboaQuality.setRequestDate(LocalDateTime.now());
        lisboaQuality.setData(lisboaData);

        lisboaQuality.setApiName("Weather");
        lisboaCoord.setLocale("Lisboa");
        lisboaQuality.setCoordinates(lisboaCoord);
        lisboaQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 1, 0, 0));
        lisboaQuality.setData(lisboaData);

        String apiLisboa = "Weather";
        String reqTypeLisboa = "FUTURE";

        int startingHits = localCache.getHits();
        int startingMisses = localCache.getMisses();
        int startingRequests = localCache.getTotalRequests();

        AirResponse response = localCache.getFromCache(espinhoCoord, apiEspinho, reqTypeEspinho);

        assertThat(response.getApiName()).isEqualTo(espinhoQuality.getApiName());
        assertThat(response.getCoordinates()).isEqualTo(espinhoQuality.getCoordinates());
        assertThat(response.getData()).isEqualTo(espinhoQuality.getData());

        response = localCache.getFromCache(lisboaCoord, apiLisboa, reqTypeLisboa);

        assertThat(response.getApiName()).isEqualTo(lisboaQuality.getApiName());
        assertThat(response.getCoordinates()).isEqualTo(lisboaQuality.getCoordinates());
        assertThat(response.getData()).isEqualTo(lisboaQuality.getData());

        assertThat(localCache.getHits()).isEqualTo(startingHits + 2);
        assertThat(localCache.getMisses()).isEqualTo(startingMisses);
        assertThat(localCache.getTotalRequests()).isEqualTo(startingRequests + 2);
    }

    @Test
    public void whenInvalidItem_andGivenAPI_thenReturnNull__andIncrementMissesAndRequests() {
        Coordinates nonExistantCoord = new Coordinates(8.6410, 41.0072);

        String api = "Weather";
        String reqType = "PAST";

        int startingHits = localCache.getHits();
        int startingMisses = localCache.getMisses();
        int startingRequests = localCache.getTotalRequests();

        AirResponse response = localCache.getFromCache(nonExistantCoord, api, reqType);
        assertThat(response).isNull();

        nonExistantCoord = new Coordinates(7.6410, 45.0072);
        api = "Breeze";
        reqType = "CURRENT";

        response = localCache.getFromCache(nonExistantCoord, api, reqType);
        assertThat(response).isNull();

        assertThat(localCache.getHits()).isEqualTo(startingHits);
        assertThat(localCache.getMisses()).isEqualTo(startingMisses + 2);
        assertThat(localCache.getTotalRequests()).isEqualTo(startingRequests + 2);
    }

    @Test
    public void whenValidItem_andGivenNoAPI_thenReturnCorrectItem_andIncrementHitsAndRequests() {
        AirResponse espinhoQuality = new AirResponse();
        Coordinates espinhoCoord = new Coordinates(8.6410, 41.0072);

        Map<Integer, AirQualityItem> espinhoData = new HashMap<>();
        BreezeAirQuality testEspinho = new BreezeAirQuality();
        testEspinho.setBaqi(69);
        espinhoData.put(0, testEspinho);

        espinhoQuality.setApiName("Breeze");
        espinhoQuality.setCoordinates(espinhoCoord);
        espinhoQuality.setData(espinhoData);

        String reqTypeEspinho = "CURRENT";

        AirResponse lisboaQuality = new AirResponse();
        Coordinates lisboaCoord = new Coordinates(9.1393, 38.7223);
        Map<Integer, AirQualityItem> lisboaData = new HashMap<>();
        WeatherAirQuality testLisboa = new WeatherAirQuality();
        testLisboa.setAqi(5);
        lisboaData.put(0, testLisboa);
        testLisboa.setAqi(10);
        lisboaData.put(1, testLisboa);

        lisboaQuality.setApiName("Weather");
        lisboaCoord.setLocale("Lisboa");
        lisboaQuality.setCoordinates(lisboaCoord);
        lisboaQuality.setRequestDate(LocalDateTime.now());
        lisboaQuality.setData(lisboaData);

        lisboaCoord.setLocale("Lisboa");
        lisboaQuality.setCoordinates(lisboaCoord);
        lisboaQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 1, 0, 0));
        lisboaQuality.setData(lisboaData);

        String apiLisboa = "Weather";
        String reqTypeLisboa = "FUTURE";

        int startingHits = localCache.getHits();
        int startingMisses = localCache.getMisses();
        int startingRequests = localCache.getTotalRequests();

        String api = null;

        AirResponse response = localCache.getFromCache(espinhoCoord, api, reqTypeEspinho);

        assertThat(response.getApiName()).isEqualTo(espinhoQuality.getApiName());
        assertThat(response.getCoordinates()).isEqualTo(espinhoQuality.getCoordinates());
        assertThat(response.getData()).isEqualTo(espinhoQuality.getData());

        response = localCache.getFromCache(lisboaCoord, api, reqTypeLisboa);

        assertThat(response.getApiName()).isEqualTo(lisboaQuality.getApiName());
        assertThat(response.getCoordinates()).isEqualTo(lisboaQuality.getCoordinates());
        assertThat(response.getData()).isEqualTo(lisboaQuality.getData());

        assertThat(localCache.getHits()).isEqualTo(startingHits + 2);
        assertThat(localCache.getMisses()).isEqualTo(startingMisses);
        assertThat(localCache.getTotalRequests()).isEqualTo(startingRequests + 2);
    }

    @Test
    public void whenInvalidItem_andGivenNoAPI_thenReturnNull__andIncrementMissesAndRequests() {
        Coordinates nonExistantCoord = new Coordinates(8.6410, 41.0072);

        String api = null;
        String reqType = "PAST";

        int startingHits = localCache.getHits();
        int startingMisses = localCache.getMisses();
        int startingRequests = localCache.getTotalRequests();

        AirResponse response = localCache.getFromCache(nonExistantCoord, api, reqType);
        assertThat(response).isNull();

        nonExistantCoord = new Coordinates(7.6410, 45.0072);
        reqType = "CURRENT";

        response = localCache.getFromCache(nonExistantCoord, api, reqType);
        assertThat(response).isNull();

        assertThat(localCache.getHits()).isEqualTo(startingHits);
        assertThat(localCache.getMisses()).isEqualTo(startingMisses + 2);
        assertThat(localCache.getTotalRequests()).isEqualTo(startingRequests + 2);
    }

    @Test
    public void whenValidItem_andGivenAPI_butWrongAPI_thenReturnNull() {
        AirResponse espinhoQuality = new AirResponse();
        Coordinates espinhoCoord = new Coordinates(8.6410, 41.0072);

        Map<Integer, AirQualityItem> espinhoData = new HashMap<>();
        BreezeAirQuality testEspinho = new BreezeAirQuality();
        testEspinho.setBaqi(69);
        espinhoData.put(0, testEspinho);

        espinhoQuality.setApiName("Breeze");
        espinhoQuality.setCoordinates(espinhoCoord);
        espinhoQuality.setData(espinhoData);

        String apiEspinho = "Breeze";
        String reqTypeEspinho = "CURRENT";

        AirResponse lisboaQuality = new AirResponse();
        Coordinates lisboaCoord = new Coordinates(9.1393, 38.7223);
        Map<Integer, AirQualityItem> lisboaData = new HashMap<>();
        WeatherAirQuality testLisboa = new WeatherAirQuality();
        testLisboa.setAqi(5);
        lisboaData.put(0, testLisboa);
        testLisboa.setAqi(10);
        lisboaData.put(1, testLisboa);

        lisboaQuality.setApiName("Weather");
        lisboaCoord.setLocale("Lisboa");
        lisboaQuality.setCoordinates(lisboaCoord);
        lisboaQuality.setRequestDate(LocalDateTime.now());
        lisboaQuality.setData(lisboaData);

        lisboaCoord.setLocale("Lisboa");
        lisboaQuality.setCoordinates(lisboaCoord);
        lisboaQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 1, 0, 0));
        lisboaQuality.setData(lisboaData);

        String apiLisboa = "Weather";
        String reqTypeLisboa = "FUTURE";

        int startingHits = localCache.getHits();
        int startingMisses = localCache.getMisses();
        int startingRequests = localCache.getTotalRequests();


        AirResponse response = localCache.getFromCache(espinhoCoord, apiLisboa, reqTypeEspinho);

        assertThat(response).isNull();

        response = localCache.getFromCache(lisboaCoord, apiEspinho, reqTypeLisboa);

        assertThat(response).isNull();

        assertThat(localCache.getHits()).isEqualTo(startingHits);
        assertThat(localCache.getMisses()).isEqualTo(startingMisses + 2);
        assertThat(localCache.getTotalRequests()).isEqualTo(startingRequests + 2);
    }

    @Test
    public void whenValidItem_butWrongReqType_thenReturnNull() {
        AirResponse espinhoQuality = new AirResponse();
        Coordinates espinhoCoord = new Coordinates(8.6410, 41.0072);

        Map<Integer, AirQualityItem> espinhoData = new HashMap<>();
        BreezeAirQuality testEspinho = new BreezeAirQuality();
        testEspinho.setBaqi(69);
        espinhoData.put(0, testEspinho);

        espinhoQuality.setApiName("Breeze");
        espinhoQuality.setCoordinates(espinhoCoord);
        espinhoQuality.setData(espinhoData);

        String apiEspinho = "Breeze";
        String reqTypeEspinho = "CURRENT";

        AirResponse lisboaQuality = new AirResponse();
        Coordinates lisboaCoord = new Coordinates(9.1393, 38.7223);
        Map<Integer, AirQualityItem> lisboaData = new HashMap<>();
        WeatherAirQuality testLisboa = new WeatherAirQuality();
        testLisboa.setAqi(5);
        lisboaData.put(0, testLisboa);
        testLisboa.setAqi(10);
        lisboaData.put(1, testLisboa);

        lisboaQuality.setApiName("Weather");
        lisboaCoord.setLocale("Lisboa");
        lisboaQuality.setCoordinates(lisboaCoord);
        lisboaQuality.setRequestDate(LocalDateTime.now());
        lisboaQuality.setData(lisboaData);

        lisboaCoord.setLocale("Lisboa");
        lisboaQuality.setCoordinates(lisboaCoord);
        lisboaQuality.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 1, 0, 0));
        lisboaQuality.setData(lisboaData);

        String apiLisboa = "Weather";
        String reqTypeLisboa = "FUTURE";

        int startingHits = localCache.getHits();
        int startingMisses = localCache.getMisses();
        int startingRequests = localCache.getTotalRequests();


        AirResponse response = localCache.getFromCache(espinhoCoord, apiEspinho, reqTypeLisboa);

        assertThat(response).isNull();

        response = localCache.getFromCache(lisboaCoord, apiLisboa, reqTypeEspinho);

        assertThat(response).isNull();

        assertThat(localCache.getHits()).isEqualTo(startingHits);
        assertThat(localCache.getMisses()).isEqualTo(startingMisses + 2);
        assertThat(localCache.getTotalRequests()).isEqualTo(startingRequests + 2);
    }

    @Test
    public void whenValidItem_andItemIsExpired_thenReturnNull_andIncrementMissesAndRequests_andRemoveFromCache() {
        AirResponse aveiroQuality = new AirResponse();
        Coordinates aveiroCoord = new Coordinates(8.6538, 40.6405);
        Map<Integer, AirQualityItem> aveiroData = new HashMap<>();

        aveiroQuality.setApiName("Weather");
        aveiroCoord.setLocale("Aveiro");
        aveiroQuality.setCoordinates(aveiroCoord);
        aveiroQuality.setRequestDate(LocalDateTime.of(1980, 5, 7, 20, 1, 42, 0));   // A date that is way over 2 minutes from the corrent one
        aveiroQuality.setData(aveiroData);

        int startingSize = localCache.getSize();

        localCache.addToCache(aveiroQuality, "CURRENT");

        assertThat(localCache.getSize()).isEqualTo(startingSize + 1); // Make sure we added


        int startingHits = localCache.getHits();
        int startingMisses = localCache.getMisses();
        int startingRequests = localCache.getTotalRequests();

        AirResponse response = localCache.getFromCache(aveiroCoord, null, "CURRENT");
        assertThat(response).isNull();

        assertThat(localCache.getSize()).isEqualTo(startingSize);
        assertThat(localCache.getHits()).isEqualTo(startingHits);
        assertThat(localCache.getMisses()).isEqualTo(startingMisses + 1);
        assertThat(localCache.getTotalRequests()).isEqualTo(startingRequests + 1);
    }

    @Test
    public void whenCacheNotFull_addToCache_andIncrementSize(){
        AirResponse aveiroQuality = new AirResponse();
        Coordinates aveiroCoord = new Coordinates(8.6538, 40.6405);
        Map<Integer, AirQualityItem> aveiroData = new HashMap<>();

        aveiroQuality.setApiName("Weather");
        aveiroCoord.setLocale("Aveiro");
        aveiroQuality.setCoordinates(aveiroCoord);
        aveiroQuality.setRequestDate(LocalDateTime.now());   // A date that is way over 2 minutes from the corrent one
        aveiroQuality.setData(aveiroData);

        int startingSize = localCache.getSize();

        localCache.addToCache(aveiroQuality, "CURRENT");

        assertThat(localCache.getSize()).isEqualTo(startingSize+1);
        assertThat(localCache.getCache().values()).contains(aveiroQuality);
    }

    @Test
    public void whenCacheFull_removeOldest_andAddNew(){
        AirResponse aveiroQuality = new AirResponse();
        Coordinates aveiroCoord = new Coordinates(8.6538, 40.6405);
        Map<Integer, AirQualityItem> aveiroData = new HashMap<>();

        aveiroQuality.setApiName("Weather");
        aveiroCoord.setLocale("Aveiro");
        aveiroQuality.setCoordinates(aveiroCoord);
        aveiroQuality.setRequestDate(LocalDateTime.now());   // A date that is way over 2 minutes from the corrent one
        aveiroQuality.setData(aveiroData);

        Coordinates espinhoCoord = new Coordinates(8.6410, 41.0072); // Coordinates of the oldest item

        int startingSize = localCache.getSize();

        // Fill Cache
        int maxSize = localCache.getMaxCacheSize();
        for(int i = startingSize ; i < maxSize ; i++){
            AirResponse inc = new AirResponse();
            Coordinates coor = new Coordinates(i,i);
            inc.setCoordinates(coor);
            localCache.addToCache(inc, "CURRENT");
        }

        assertThat(localCache.getSize()).isEqualTo(maxSize);

        localCache.addToCache(aveiroQuality, "CURRENT");

        assertThat(localCache.getCache().values()).hasSize(maxSize).extracting(AirResponse::getCoordinates).doesNotContain(espinhoCoord);
        assertThat(localCache.getCache().values()).contains(aveiroQuality);
        assertThat(localCache.getCacheQueue().peek().getCoords()).isNotEqualTo(espinhoCoord);
    }
}