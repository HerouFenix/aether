package tqs.air.aether.api;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;

import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import org.mockito.Mockito;

import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.BDDMockito.given;

import org.springframework.web.client.RestClientResponseException;
import tqs.air.aether.api.info.AirResponse;
import tqs.air.aether.api.locale.Locale;
import tqs.air.aether.api.locale.LocalePOJO;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@WebMvcTest(AetherRestController.class)
class AetherControllerIT {
    @Autowired
    private MockMvc mvc; // Get a reference to the server context

    @MockBean
    private AetherService service; // Mock dependency to the service class

    @Test
    // Tests that when creating a new, non-existent locale, it gets created
    public void whenPostNewLocale_thenCreateLocale() throws Exception {
        Locale aveiro = new Locale("Aveiro", 8.6538, 40.6405);
        given(service.createLocale(Mockito.any(LocalePOJO.class))).willReturn(aveiro);

        mvc.perform(post("/api/locale").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(aveiro)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is("Aveiro")))
                .andExpect(jsonPath("$.lat", is(8.6538)))
                .andExpect(jsonPath("$.lon", is(40.6405)));

        verify(service, VerificationModeFactory.times(1)).createLocale(Mockito.any(LocalePOJO.class));
        reset(service);
    }


    @Test
    // Tests that when updating an existing locale it gets updated
    public void whenUpdatingExistingLocale_thenUpdateLocale() throws Exception {
        Locale aveiro = new Locale("Aveiro", 8.6538, 41.6405);
        given(service.updateLocale(Mockito.any(LocalePOJO.class))).willReturn(aveiro);

        mvc.perform(put("/api/locale").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(aveiro)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Aveiro")))
                .andExpect(jsonPath("$.lat", is(8.6538)))
                .andExpect(jsonPath("$.lon", is(41.6405)));

        verify(service, VerificationModeFactory.times(1)).updateLocale(Mockito.any(LocalePOJO.class));
        reset(service);
    }


    @Test
    // Tests that when given a valid locale, it deletes it
    public void whenDeleteLocale_thenDeleteLocale() throws Exception {
        Locale aveiro = new Locale("Aveiro", 8.6538, 40.6405);
        given(service.deleteLocale(Mockito.anyString())).willReturn(aveiro);

        mvc.perform(delete("/api/locale").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Aveiro")))
                .andExpect(jsonPath("$.lat", is(8.6538)))
                .andExpect(jsonPath("$.lon", is(40.6405)));

        verify(service, VerificationModeFactory.times(1)).deleteLocale(Mockito.anyString());
        reset(service);
    }


    @Test
    // Tests that when fetching all locales, all locales get returned
    public void givenLocales_whenGetLocales_thenReturnJsonArray() throws Exception {
        Locale aveiro = new Locale("Aveiro", 8.6538, 40.6405);
        Locale porto = new Locale("Porto", 8.6291, 41.1579);
        Locale lisboa = new Locale("Lisboa", 9.1393, 38.7223);

        List<Locale> allLocales = Arrays.asList(aveiro, porto, lisboa);

        given(service.getAllLocales()).willReturn(allLocales);

        mvc.perform(get("/api/locale").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].name", is(aveiro.getName())))
                .andExpect(jsonPath("$[1].name", is(porto.getName())))
                .andExpect(jsonPath("$[2].name", is(lisboa.getName())));

        verify(service, VerificationModeFactory.times(1)).getAllLocales();
        reset(service);
    }

    @Test
    // Tests that when fetching cache we get returned the cache
    public void givenCacheItems_whenGetCache_thenReturnCache() throws Exception {
        AirResponse aveiroQuality = new AirResponse();
        aveiroQuality.setApiName("Test1");

        AirResponse espinhoQuality = new AirResponse();
        aveiroQuality.setApiName("Test2");

        List<AirResponse> cache = Arrays.asList(espinhoQuality, aveiroQuality);

        given(service.getEntireCache()).willReturn(cache);

        mvc.perform(get("/api/cache/items").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].apiName", is(espinhoQuality.getApiName())))
                .andExpect(jsonPath("$[1].apiName", is(aveiroQuality.getApiName())));


        verify(service, VerificationModeFactory.times(1)).getEntireCache();
        reset(service);
    }


    @Test
    // Tests that when fetching cache size we get the cache's size
    public void givenCacheSize_whenGetCacheSize_thenReturnCacheSize() throws Exception {
        int cacheSize = 5;
        given(service.getCacheSize()).willReturn(cacheSize);

        mvc.perform(get("/api/cache/size").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(cacheSize)));


        verify(service, VerificationModeFactory.times(1)).getCacheSize();
        reset(service);
    }


    @Test
    // Tests that when fetching cache we get returned the cache
    public void givenCacheHitsNMisses_whenGetHitsNMisses_thenReturnHitsNMisses() throws Exception {
        int hits = 5;
        int misses = 3;
        int totalReqs = hits + misses;

        Map<String, Integer> hitsnmisses = new HashMap<>();
        hitsnmisses.put("Hits", hits);
        hitsnmisses.put("Misses", misses);
        hitsnmisses.put("TotalRequests", totalReqs);

        given(service.getHitsNMisses()).willReturn(hitsnmisses);

        mvc.perform(get("/api/cache/hitsnmisses").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.TotalRequests", is(totalReqs)))
                .andExpect(jsonPath("$.Hits", is(hits)))
                .andExpect(jsonPath("$.Misses", is(misses)));


        verify(service, VerificationModeFactory.times(1)).getHitsNMisses();
        reset(service);
    }


    @Test
    // Tests that when given a locale, we get returned the air quality
    public void givenLocale_getAirQuality() throws Exception {
        AirResponse aveiroQuality = new AirResponse();
        aveiroQuality.setApiName("TEST");

        String locale = "Aveiro";

        String reqType = "CURRENT";

        given(service.getQualityByLocale(Mockito.anyString(), Mockito.anyString(), Mockito.any())).willReturn(aveiroQuality);

        // Past
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("locale", locale))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is(aveiroQuality.getApiName())));

        verify(service, VerificationModeFactory.times(1)).getQualityByLocale(locale, reqType, null);

        reset(service);
    }

    @Test
    // Tests that when given a locale, we get returned the air quality
    public void givenLocale_andAPI_getAirQuality_andAssureAPIIsCorrect() throws Exception {
        AirResponse aveiroQuality = new AirResponse();

        String locale = "Aveiro";
        String api = "Weather";
        aveiroQuality.setApiName(api);

        String reqType = "CURRENT";

        given(service.getQualityByLocale(Mockito.anyString(), Mockito.anyString(), Mockito.any())).willReturn(aveiroQuality);

        // Past
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("locale", locale).param("api", api))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is(aveiroQuality.getApiName())));

        verify(service, VerificationModeFactory.times(1)).getQualityByLocale(locale, reqType, api);

        reset(service);
    }

    @Test
    // Tests that when given coordinates, we get returned an air quality object
    public void givenCoordinates_getAirQuality() throws Exception {
        AirResponse aveiroQuality = new AirResponse();
        aveiroQuality.setApiName("TEST");
        double lat = 8.6538;
        double lon = 40.6405;

        String reqType = "CURRENT";

        given(service.getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.any(), Mockito.any(), Mockito.any())).willReturn(aveiroQuality);

        // Past
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", lat + "").param("lon", lon + ""))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is(aveiroQuality.getApiName())));

        verify(service, VerificationModeFactory.times(1)).getQualityByCoord(lat, lon, reqType, null, null);

        reset(service);
    }


    @Test
    // Tests that when the breezeometer api returns an error, we get returned a ResponseStatusException
    public void givenBreezeError_getErrorMessage() throws Exception {
        double lat = 8.6538;
        double lon = 40.6405;

        String reqType = "CURRENT";

        String expectedErrorMessage = "Mock Error raised by BreezeOMeter API";

        given(service.getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.any(), Mockito.any())).willThrow(new RestClientResponseException("Mock", 400, "Mock", HttpHeaders.EMPTY, "{\"error\": \"Mock Error raised by BreezeOMeter API\"}".getBytes(), Charset.defaultCharset()));

        // Past
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", lat + "").param("lon", lon + ""))
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason(is(expectedErrorMessage)));

        verify(service, VerificationModeFactory.times(1)).getQualityByCoord(lat, lon, reqType, null, null);

        reset(service);
    }


    @Test
    // Tests that when the weatherbit api returns an error, we get returned a ResponseStatusException
    public void givenWeatherBitError_getErrorMessage() throws Exception {
        given(service.getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.any(), Mockito.any())).willThrow(new RestClientResponseException("Mock", 400, "Mock", HttpHeaders.EMPTY, "{\"error\": \"Mock Error raised by WeatherBit API\"}".getBytes(), Charset.defaultCharset()));

        double lat = 8.6538;
        double lon = 40.6405;

        String reqType = "CURRENT";

        String expectedErrorMessage = "Mock Error raised by WeatherBit API";

        // Past
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", lat + "").param("lon", lon + ""))
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason(is(expectedErrorMessage)));

        verify(service, VerificationModeFactory.times(1)).getQualityByCoord(lat, lon, reqType, null, null);

        reset(service);
    }


    @Test
    // Tests that when given an illegal api name, we get returned a ResponseStatusException
    public void givenBadAPI_getErrorMessage() throws Exception {
        String badApiName = "Oof";

        double lat = 8.6538;
        double lon = 40.6405;

        String reqType = "CURRENT";

        String expectedErrorMessage = "Unexpected API value : Oof . Was expecting 'Breeze', 'Weather' or nothing";

        given(service.getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.matches("Oof"), Mockito.any())).willThrow(new IllegalStateException("Unexpected API value : " + badApiName + " . Was expecting 'Breeze', 'Weather' or nothing"));

        // Past
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", lat + "").param("lon", lon + "").param("api", badApiName))
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason(is(expectedErrorMessage)));

        verify(service, VerificationModeFactory.times(1)).getQualityByCoord(lat, lon, reqType, badApiName, null);
        reset(service);
    }


    @Test
    // Tests that when given a locale, we get returned the air quality
    public void givenLocale_getAirQuality_Past() throws Exception {
        AirResponse aveiroQuality = new AirResponse();
        aveiroQuality.setApiName("TEST");

        String locale = "Aveiro";

        String reqType = "PAST";

        given(service.getQualityByLocale(Mockito.anyString(), Mockito.anyString(), Mockito.any())).willReturn(aveiroQuality);

        // Past
        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("locale", locale))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is(aveiroQuality.getApiName())));

        verify(service, VerificationModeFactory.times(1)).getQualityByLocale(locale, reqType, null);

        reset(service);
    }

    @Test
    // Tests that when given a locale, we get returned the air quality
    public void givenLocale_andAPI_getAirQuality_andAssureAPIIsCorrect_Past() throws Exception {
        AirResponse aveiroQuality = new AirResponse();

        String locale = "Aveiro";
        String api = "Weather";
        aveiroQuality.setApiName(api);

        String reqType = "PAST";

        given(service.getQualityByLocale(Mockito.anyString(), Mockito.anyString(), Mockito.any())).willReturn(aveiroQuality);

        // Past
        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("locale", locale).param("api", api))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is(aveiroQuality.getApiName())));

        verify(service, VerificationModeFactory.times(1)).getQualityByLocale(locale, reqType, api);

        reset(service);
    }

    @Test
    // Tests that when given coordinates, we get returned an air quality object
    public void givenCoordinates_getAirQuality_Past() throws Exception {
        AirResponse aveiroQuality = new AirResponse();
        aveiroQuality.setApiName("TEST");
        double lat = 8.6538;
        double lon = 40.6405;

        String reqType = "PAST";

        given(service.getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.any(), Mockito.any(), Mockito.any())).willReturn(aveiroQuality);

        // Past
        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("lat", lat + "").param("lon", lon + ""))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is(aveiroQuality.getApiName())));

        verify(service, VerificationModeFactory.times(1)).getQualityByCoord(lat, lon, reqType, null, null);

        reset(service);
    }


    @Test
    // Tests that when the breezeometer api returns an error, we get returned a ResponseStatusException
    public void givenBreezeError_getErrorMessage_Past() throws Exception {
        double lat = 8.6538;
        double lon = 40.6405;

        String reqType = "PAST";

        String expectedErrorMessage = "Mock Error raised by BreezeOMeter API";

        given(service.getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.any(), Mockito.any())).willThrow(new RestClientResponseException("Mock", 400, "Mock", HttpHeaders.EMPTY, "{\"error\": {\"detail\": \"Mock Error raised by BreezeOMeter API\"}}".getBytes(), Charset.defaultCharset()));

        // Past
        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("lat", lat + "").param("lon", lon + ""))
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason(is(expectedErrorMessage)));

        verify(service, VerificationModeFactory.times(1)).getQualityByCoord(lat, lon, reqType, null, null);

        reset(service);
    }


    @Test
    // Tests that when the weatherbit api returns an error, we get returned a ResponseStatusException
    public void givenWeatherBitError_getErrorMessage_Past() throws Exception {
        given(service.getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.any(), Mockito.any())).willThrow(new RestClientResponseException("Mock", 400, "Mock", HttpHeaders.EMPTY, "{\"error\": \"Mock Error raised by WeatherBit API\"}".getBytes(), Charset.defaultCharset()));

        double lat = 8.6538;
        double lon = 40.6405;

        String reqType = "PAST";

        String expectedErrorMessage = "Mock Error raised by WeatherBit API";

        // Past
        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("lat", lat + "").param("lon", lon + ""))
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason(is(expectedErrorMessage)));

        verify(service, VerificationModeFactory.times(1)).getQualityByCoord(lat, lon, reqType, null, null);

        reset(service);
    }


    @Test
    // Tests that when given an illegal api name, we get returned a ResponseStatusException
    public void givenBadAPI_getErrorMessage_Past() throws Exception {
        String badApiName = "Oof";

        double lat = 8.6538;
        double lon = 40.6405;

        String reqType = "PAST";

        String expectedErrorMessage = "Unexpected API value : Oof . Was expecting 'Breeze', 'Weather' or nothing";

        given(service.getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.matches("Oof"), Mockito.any())).willThrow(new IllegalStateException("Unexpected API value : " + badApiName + " . Was expecting 'Breeze', 'Weather' or nothing"));

        // Past
        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("lat", lat + "").param("lon", lon + "").param("api", badApiName))
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason(is(expectedErrorMessage)));

        verify(service, VerificationModeFactory.times(1)).getQualityByCoord(lat, lon, reqType, badApiName, null);
        reset(service);
    }


    @Test
    // Tests that when given a locale, we get returned the air quality
    public void givenLocale_getAirQuality_Future() throws Exception {
        AirResponse aveiroQuality = new AirResponse();
        aveiroQuality.setApiName("TEST");

        String locale = "Aveiro";

        String reqType = "FUTURE";

        given(service.getQualityByLocale(Mockito.anyString(), Mockito.anyString(), Mockito.any())).willReturn(aveiroQuality);

        // Past
        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("locale", locale))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is(aveiroQuality.getApiName())));

        verify(service, VerificationModeFactory.times(1)).getQualityByLocale(locale, reqType, null);

        reset(service);
    }

    @Test
    // Tests that when given a locale, we get returned the air quality
    public void givenLocale_andAPI_getAirQuality_andAssureAPIIsCorrect_Future() throws Exception {
        AirResponse aveiroQuality = new AirResponse();

        String locale = "Aveiro";
        String api = "Weather";
        aveiroQuality.setApiName(api);

        String reqType = "FUTURE";

        given(service.getQualityByLocale(Mockito.anyString(), Mockito.anyString(), Mockito.any())).willReturn(aveiroQuality);

        // Past
        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("locale", locale).param("api", api))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is(aveiroQuality.getApiName())));

        verify(service, VerificationModeFactory.times(1)).getQualityByLocale(locale, reqType, api);

        reset(service);
    }

    @Test
    // Tests that when given coordinates, we get returned an air quality object
    public void givenCoordinates_getAirQuality_Future() throws Exception {
        AirResponse aveiroQuality = new AirResponse();
        aveiroQuality.setApiName("TEST");
        double lat = 8.6538;
        double lon = 40.6405;

        String reqType = "FUTURE";

        given(service.getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.any(), Mockito.any(), Mockito.any())).willReturn(aveiroQuality);

        // Past
        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("lat", lat + "").param("lon", lon + ""))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is(aveiroQuality.getApiName())));

        verify(service, VerificationModeFactory.times(1)).getQualityByCoord(lat, lon, reqType, null, null);

        reset(service);
    }


    @Test
    // Tests that when the breezeometer api returns an error, we get returned a ResponseStatusException
    public void givenBreezeError_getErrorMessage_Future() throws Exception {
        double lat = 8.6538;
        double lon = 40.6405;

        String reqType = "FUTURE";

        String expectedErrorMessage = "Mock Error raised by BreezeOMeter API";

        given(service.getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.any(), Mockito.any())).willThrow(new RestClientResponseException("Mock", 400, "Mock", HttpHeaders.EMPTY, "{\"error\": {\"detail\": \"Mock Error raised by BreezeOMeter API\"}}".getBytes(), Charset.defaultCharset()));

        // Past
        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("lat", lat + "").param("lon", lon + ""))
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason(is(expectedErrorMessage)));

        verify(service, VerificationModeFactory.times(1)).getQualityByCoord(lat, lon, reqType, null, null);

        reset(service);
    }


    @Test
    // Tests that when the weatherbit api returns an error, we get returned a ResponseStatusException
    public void givenWeatherBitError_getErrorMessage_Future() throws Exception {
        given(service.getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.any(), Mockito.any())).willThrow(new RestClientResponseException("Mock", 400, "Mock", HttpHeaders.EMPTY, "{\"error\": \"Mock Error raised by WeatherBit API\"}".getBytes(), Charset.defaultCharset()));

        double lat = 8.6538;
        double lon = 40.6405;

        String reqType = "FUTURE";

        String expectedErrorMessage = "Mock Error raised by WeatherBit API";

        // Past
        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("lat", lat + "").param("lon", lon + ""))
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason(is(expectedErrorMessage)));

        verify(service, VerificationModeFactory.times(1)).getQualityByCoord(lat, lon, reqType, null, null);

        reset(service);
    }


    @Test
    // Tests that when given an illegal api name, we get returned a ResponseStatusException
    public void givenBadAPI_getErrorMessage_Future() throws Exception {
        String badApiName = "Oof";

        double lat = 8.6538;
        double lon = 40.6405;

        String reqType = "FUTURE";

        String expectedErrorMessage = "Unexpected API value : Oof . Was expecting 'Breeze', 'Weather' or nothing";

        given(service.getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.matches("Oof"), Mockito.any())).willThrow(new IllegalStateException("Unexpected API value : " + badApiName + " . Was expecting 'Breeze', 'Weather' or nothing"));

        // Past
        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("lat", lat + "").param("lon", lon + "").param("api", badApiName))
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason(is(expectedErrorMessage)));

        verify(service, VerificationModeFactory.times(1)).getQualityByCoord(lat, lon, reqType, badApiName, null);
        reset(service);
    }

    @Test
    public void whenUnexpectedError_throwError() throws Exception {
        double lat = 8.6538;
        double lon = 40.6405;
        String locale = "locale";
        String expectedErrorMessage = "An unexpected error has occurred. This was most likely caused by our external APIs. Please try again in 5-10 seconds or contact us!";

        given(service.getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.any(), Mockito.any())).willAnswer(invocation -> {
            throw new Exception("Unexpected");
        });
        ;
        given(service.getQualityByLocale(Mockito.anyString(), Mockito.anyString(), Mockito.any())).willAnswer(invocation -> {
            throw new Exception("Unexpected 2");
        });
        ;

        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", lat + "").param("lon", lon + ""))
                .andExpect(status().is5xxServerError())
                .andExpect(status().reason(is(expectedErrorMessage)));

        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("locale", locale))
                .andExpect(status().is5xxServerError())
                .andExpect(status().reason(is(expectedErrorMessage)));

        verify(service, VerificationModeFactory.times(1)).getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.any(), Mockito.any());
        verify(service, VerificationModeFactory.times(1)).getQualityByLocale(Mockito.anyString(), Mockito.anyString(), Mockito.any());
        reset(service);
    }

    @Test
    public void whenIllegalArgument_throwError() throws Exception {
        double lat = 8.6538;
        double lon = 40.6405;
        String locale = "locale";
        String expectedErrorMessage = "Illegal";
        String expectedErrorMessage2 = "Illegal 2";

        given(service.getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.any(), Mockito.any())).willAnswer(invocation -> {
            throw new IllegalStateException("Illegal");
        });
        given(service.getQualityByLocale(Mockito.anyString(), Mockito.anyString(), Mockito.any())).willAnswer(invocation -> {
            throw new IllegalStateException("Illegal 2");
        });

        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", lat + "").param("lon", lon + ""))
                .andExpect(status().is4xxClientError())
                .andExpect(status().isBadRequest())
                .andExpect(status().reason(is(expectedErrorMessage)));

        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("locale", locale))
                .andExpect(status().is4xxClientError())
                .andExpect(status().isBadRequest())
                .andExpect(status().reason(is(expectedErrorMessage2)));

        verify(service, VerificationModeFactory.times(1)).getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.any(), Mockito.any());
        verify(service, VerificationModeFactory.times(1)).getQualityByLocale(Mockito.anyString(), Mockito.anyString(), Mockito.any());
        reset(service);
    }

    @Test
    public void whenAPIError_throwError() throws Exception {
        double lat = 8.6538;
        double lon = 40.6405;
        String locale = "locale";
        String expectedErrorMessage = "API Error";
        String expectedErrorMessage2 = "API Error 2";

        given(service.getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.any(), Mockito.any())).willAnswer(invocation -> {
            throw new RestClientResponseException("Mock", 400, "Mock", HttpHeaders.EMPTY, "{\"error\": \"API Error\"}".getBytes(), Charset.defaultCharset());
        });
        ;
        given(service.getQualityByLocale(Mockito.anyString(), Mockito.anyString(), Mockito.any())).willAnswer(invocation -> {
            throw new RestClientResponseException("Mock", 400, "Mock", HttpHeaders.EMPTY, "{\"error\": \"API Error 2\"}".getBytes(), Charset.defaultCharset());
        });

        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", lat + "").param("lon", lon + ""))
                .andExpect(status().is4xxClientError())
                .andExpect(status().isBadRequest())
                .andExpect(status().reason(is(expectedErrorMessage)));

        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("locale", locale))
                .andExpect(status().is4xxClientError())
                .andExpect(status().isBadRequest())
                .andExpect(status().reason(is(expectedErrorMessage2)));

        verify(service, VerificationModeFactory.times(1)).getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.any(), Mockito.any());
        verify(service, VerificationModeFactory.times(1)).getQualityByLocale(Mockito.anyString(), Mockito.anyString(), Mockito.any());
        reset(service);
    }

    @Test
    public void whenError_whenTreatingAPIError_throwError() throws Exception {
        double lat = 8.6538;
        double lon = 40.6405;
        String locale = "locale";
        String expectedErrorMessage = "Oof";
        String expectedErrorMessage2 = "Oof 2";

        given(service.getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.any(), Mockito.any())).willAnswer(invocation -> {
            throw new RestClientResponseException("Mock", 400, "Mock", HttpHeaders.EMPTY, "Oof".getBytes(), Charset.defaultCharset());
        });
        ;
        given(service.getQualityByLocale(Mockito.anyString(), Mockito.anyString(), Mockito.any())).willAnswer(invocation -> {
            throw new RestClientResponseException("Mock", 400, "Mock", HttpHeaders.EMPTY, "Oof 2".getBytes(), Charset.defaultCharset());
        });

        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", lat + "").param("lon", lon + ""))
                .andExpect(status().is4xxClientError())
                .andExpect(status().isBadRequest())
                .andExpect(status().reason(is(expectedErrorMessage)));

        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("locale", locale))
                .andExpect(status().is4xxClientError())
                .andExpect(status().isBadRequest())
                .andExpect(status().reason(is(expectedErrorMessage2)));

        verify(service, VerificationModeFactory.times(1)).getQualityByCoord(Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.any(), Mockito.any());
        verify(service, VerificationModeFactory.times(1)).getQualityByLocale(Mockito.anyString(), Mockito.anyString(), Mockito.any());
        reset(service);
    }

}