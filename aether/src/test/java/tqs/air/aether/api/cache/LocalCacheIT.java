package tqs.air.aether.api.cache;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import tqs.air.aether.api.info.AirQualityItem;
import tqs.air.aether.api.info.AirResponse;
import tqs.air.aether.api.info.Coordinates;
import tqs.air.aether.api.info.breeze.BreezeAirQuality;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.*;

@SpringJUnitConfig(LocalCache.class)
public class LocalCacheIT {
    @Autowired
    @SpyBean
    private LocalCache localCache;

    @BeforeEach
    public void setUp() {
        localCache.resetCache();
        reset(localCache);
    }


    @Test
    public void whenTimeout_andEmpty_doNotRemove() throws InterruptedException {
        int startingSize = localCache.getSize();

        // Wait until remove from cache is called
        long scheduleTime = localCache.getTimeout() + 1000;

        await().atMost(Duration.ofMillis(scheduleTime)).untilAsserted(() -> verify(localCache, atLeast(1)).removeFromCache());

        int endingSize = localCache.getSize();
        assertThat(endingSize).isEqualTo(startingSize);
    }

    @Test
    public void everyMinute_removeGetsCalled() throws InterruptedException {
        long scheduleTime = localCache.getTimeout() + 1000;
        await().atMost(Duration.ofMillis(scheduleTime)).untilAsserted(() -> verify(localCache, VerificationModeFactory.times(1)).removeFromCache());
    }


    @Test
    public void whenTimeout_andNotEmpty_andOldestItemOlderThanTimeout_RemoveFromCache() throws InterruptedException {
        AirResponse espinhoQuality = new AirResponse();
        Coordinates espinhoCoord = new Coordinates(8.6410, 41.0072);
        Map<Integer, AirQualityItem> espinhoData = new HashMap<>();
        BreezeAirQuality testEspinho = new BreezeAirQuality();
        testEspinho.setBaqi(69);
        espinhoData.put(0, testEspinho);

        espinhoQuality.setApiName("Breeze");
        espinhoQuality.setCoordinates(espinhoCoord);
        espinhoQuality.setRequestDate(LocalDateTime.of(1980, 5, 7, 20, 0, 0, 0)); // Date way over 2 minutes
        espinhoQuality.setData(espinhoData);

        localCache.addToCache(espinhoQuality, "CURRENT");

        int startingSize = localCache.getSize();

        long scheduleTime = localCache.getTimeout() + 1000; // A bit over the timeout

        // Wait at least for the remove from cache function to be called again (shouldn't take more than 1 minute)
        await().atMost(Duration.ofMillis(scheduleTime)).untilAsserted(() -> verify(localCache, atLeast(1)).removeFromCache());

        int endingSize = localCache.getSize();
        assertThat(endingSize).isEqualTo(startingSize-1);
    }

    @Test
    public void whenTimeout_andNotEmpty_andOldestItemYoungerThan2Mins_DoNotRemove() throws InterruptedException {
        AirResponse espinhoQuality = new AirResponse();
        Coordinates espinhoCoord = new Coordinates(8.6410, 41.0072);
        Map<Integer, AirQualityItem> espinhoData = new HashMap<>();
        BreezeAirQuality testEspinho = new BreezeAirQuality();
        testEspinho.setBaqi(69);
        espinhoData.put(0, testEspinho);

        espinhoQuality.setApiName("Breeze");
        espinhoQuality.setCoordinates(espinhoCoord);
        espinhoQuality.setRequestDate(LocalDateTime.now()); // Date way over 2 minutes
        espinhoQuality.setData(espinhoData);

        localCache.addToCache(espinhoQuality, "CURRENT");

        int startingSize = localCache.getSize();
        long scheduleTime = localCache.getTimeout() + 1000;

        // Wait at least for the remove from cache function to be called again (shouldn't take more than 1 minute)
        await().atMost(Duration.ofMillis(scheduleTime)).untilAsserted(() -> verify(localCache, atLeast(1)).removeFromCache());

        int endingSize = localCache.getSize();
        assertThat(endingSize).isEqualTo(startingSize);

        assertThat(localCache.getCache().values()).contains(espinhoQuality);
    }
}
