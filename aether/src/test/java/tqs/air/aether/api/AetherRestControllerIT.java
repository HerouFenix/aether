package tqs.air.aether.api;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import tqs.air.aether.AetherApplication;
import tqs.air.aether.api.cache.LocalCache;
import tqs.air.aether.api.info.AirQualityItem;
import tqs.air.aether.api.info.AirResponse;
import tqs.air.aether.api.info.Coordinates;
import tqs.air.aether.api.info.breeze.BreezeAirQuality;
import tqs.air.aether.api.info.weatherbit.WeatherAirQuality;
import tqs.air.aether.api.locale.Locale;
import tqs.air.aether.api.locale.LocalePOJO;
import tqs.air.aether.api.locale.LocaleRepository;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;


@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = AetherApplication.class)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class AetherRestControllerIT {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private LocaleRepository localeRepository;

    @Autowired
    private LocalCache localCache;

    @AfterEach
    public void reset() {
        localeRepository.deleteAll();
        localCache.resetCache();
    }

    @Test
    // Tests that when creating a new, non-existent locale, it gets created
    public void whenValidInput_thenCreateLocale() throws Exception {
        LocalePOJO espinho = new LocalePOJO("Espinho", 8.6410, 41.0072);

        mvc.perform(post("/api/locale").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(espinho)))
                .andExpect(status().isCreated());

        Locale foundLocale = localeRepository.findByName("Espinho");
        assertThat(foundLocale.getName()).isEqualTo(espinho.getName());
        assertThat(foundLocale.getLat()).isEqualTo(espinho.getLat());
        assertThat(foundLocale.getLon()).isEqualTo(espinho.getLon());

        List<Locale> allFinds = this.localeRepository.findAll();
        assertThat(allFinds).extracting(Locale::getName).containsOnly("Espinho");
    }

    @Test
    // Tests that when creating an already existing locale, it returns an error
    public void whenInvalidInput_thenDoNotCreateLocale() throws Exception {
        double initialLon = 40.6405;
        double initialLat = 8.6538;
        this.createTestLocale("Espinho", initialLat, initialLon);

        LocalePOJO espinho = new LocalePOJO("Espinho", 8.6410, 41.0072);

        mvc.perform(post("/api/locale").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(espinho)))
                .andExpect(status().isConflict())
                .andExpect(status().reason(is("Error! There already exists a locale named: " + espinho.getName())));

        // Assure that the coordinates didn't get updated
        Locale foundLocale = localeRepository.findByName("Espinho");
        assertThat(foundLocale.getLat()).isEqualTo(initialLat);
        assertThat(foundLocale.getLon()).isEqualTo(initialLon);
    }


    @Test
    // Tests that when updating an existing locale it gets updated
    public void whenValidInput_thenUpdateLocale() throws Exception {
        this.createTestLocale("Espinho", 8.6538, 40.6405);

        double updatedLon = 41.0072;
        double updatedLat = 8.6410;
        LocalePOJO espinho = new LocalePOJO("Espinho", updatedLat, updatedLon);

        mvc.perform(put("/api/locale").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(espinho)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Espinho")))
                .andExpect(jsonPath("$.lat", is(updatedLat)))
                .andExpect(jsonPath("$.lon", is(updatedLon)));

        Locale foundLocale = localeRepository.findByName("Espinho");
        assertThat(foundLocale.getName()).isEqualTo(espinho.getName());
        assertThat(foundLocale.getLat()).isEqualTo(espinho.getLat());
        assertThat(foundLocale.getLon()).isEqualTo(espinho.getLon());
    }


    @Test
    // Tests that when updating a non existing locale, it returns an error
    public void whenInvalidInput_thenDoNotUpdateLocale() throws Exception {
        double updatedLon = 41.0072;
        double updatedLat = 8.6410;
        LocalePOJO espinho = new LocalePOJO("Espinho", updatedLat, updatedLon);

        mvc.perform(put("/api/locale").contentType(MediaType.APPLICATION_JSON).content(JsonUtil.toJson(espinho)))
                .andExpect(status().isNotFound())
                .andExpect(status().reason(is("Error! Couldn't find any locale named: " + espinho.getName())));

        Locale foundLocale = localeRepository.findByName("Espinho");
        assertThat(foundLocale).isNull();
    }


    @Test
    // Tests that when given a valid locale, it deletes it
    public void whenValidInput_thenDeleteLocale() throws Exception {
        double lat = 8.6538;
        double lon = 40.6405;
        String name = "Espinho";

        this.createTestLocale(name, lat, lon);

        // Assert that we created the entity
        Locale foundLocale = localeRepository.findByName("Espinho");
        assertThat(foundLocale.getName()).isEqualTo(name);
        assertThat(foundLocale.getLat()).isEqualTo(lat);
        assertThat(foundLocale.getLon()).isEqualTo(lon);

        mvc.perform(delete("/api/locale").contentType(MediaType.APPLICATION_JSON).param("locale", name))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(name)))
                .andExpect(jsonPath("$.lat", is(lat)))
                .andExpect(jsonPath("$.lon", is(lon)));

        // Assert that the entity was deleted
        foundLocale = localeRepository.findByName("Espinho");
        assertThat(foundLocale).isNull();
    }


    @Test
    // Tests that when given a non existent locale, it returns an error
    public void whenInvalidInput_thenDoNotDeleteLocale() throws Exception {
        String name = "Espinho";
        String expectedError = "Error! Couldn't find any locale with the specified name";

        mvc.perform(delete("/api/locale").contentType(MediaType.APPLICATION_JSON).param("locale", name))
                .andExpect(status().isNotFound())
                .andExpect(status().reason(is(expectedError)));

        Locale foundLocale = localeRepository.findByName(name);
        assertThat(foundLocale).isNull();
    }


    @Test
    // Tests that when fetching all locales, all locales get returned
    public void givenLocales_whenGetLocales_thenStatus200() throws Exception {
        this.createTestLocale("Aveiro", 8.6538, 40.6405);
        this.createTestLocale("Lisboa", 9.1393, 38.7223);
        this.createTestLocale("Porto", 8.6291, 41.1579);

        int expectedSize = 3;

        mvc.perform(get("/api/locale").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(equalTo(expectedSize))))
                .andExpect(jsonPath("$[0].name", is("Aveiro")))
                .andExpect(jsonPath("$[1].name", is("Lisboa")))
                .andExpect(jsonPath("$[2].name", is("Porto")));
    }


    @Test
    // Tests that when fetching cache we get returned the cache
    public void givenCacheItems_whenGetCache_thenStatus200() throws Exception {
        String locale = "Aveiro";
        double firstLat = 8.6538;
        double firstLon = 40.6405;
        String api = "Weather";

        double secondLat = 8.6538;
        double secondLon = 42;

        // Test Response 1
        AirResponse testResponse = new AirResponse();
        Coordinates testCoords = new Coordinates(firstLat, firstLon);
        testCoords.setLocale(locale);
        testResponse.setCoordinates(testCoords);
        testResponse.setApiName(api);
        testResponse.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 0, 0, 0));
        Map<Integer, AirQualityItem> testData = new HashMap<>();
        WeatherAirQuality testItem = new WeatherAirQuality();
        testItem.setAqi(69);
        testData.put(0, testItem);

        testResponse.setData(testData);


        String reqType = "CURRENT";

        // Test Response 2
        AirResponse testResponse2 = new AirResponse();
        Coordinates testCoords2 = new Coordinates(secondLat, secondLon);

        testResponse2.setCoordinates(testCoords2);
        testResponse2.setApiName("Breeze");
        testResponse2.setRequestDate(LocalDateTime.of(2020, 5, 7, 20, 1, 0, 0));
        Map<Integer, AirQualityItem> testData2 = new HashMap<>();
        BreezeAirQuality testItem2 = new BreezeAirQuality();
        testItem2.setBaqi(32);
        testData2.put(0, testItem2);

        testResponse2.setData(testData2);

        String reqType2 = "PAST";


        // Add items to the cache
        localCache.addToCache(testResponse, reqType);
        localCache.addToCache(testResponse2, reqType2);


        mvc.perform(get("/api/cache/items").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].coordinates.lat", is(firstLat)))
                .andExpect(jsonPath("$[0].coordinates.lon", is(firstLon)))
                .andExpect(jsonPath("$[1].coordinates.lat", is(secondLat)))
                .andExpect(jsonPath("$[1].coordinates.lon", is(secondLon)))
                .andExpect(jsonPath("$[0].coordinates.locale", is(locale)))
                .andExpect(jsonPath("$[0].apiName", is(api)));
    }

    @Test
    // Tests that when fetching cache size we get the cache's size
    public void givenCacheSize_whenGetCacheSize_thenStatus200() throws Exception {
        this.createTestLocale("Aveiro", 8.6538, 40.6405);
        int expectedSize = 2;

        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", "Weather"))
                .andExpect(status().isOk());

        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", "8.6538").param("lon", "42"))
                .andExpect(status().isOk());

        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", "8.6538").param("lon", "42"))
                .andExpect(status().isOk());

        mvc.perform(get("/api/cache/size").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is(expectedSize)));
    }


    @Test
    // Tests that when fetching cache we get returned the cache
    public void givenCacheHitsNMisses_whenGetHitsNMisses_thenStatus200() throws Exception {
        int hits = 3;
        int misses = 3;
        int totalReqs = hits + misses;

        // Miss
        this.createTestLocale("Aveiro", 8.6538, 40.6405);
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", "Weather"))
                .andExpect(status().isOk());

        // Miss
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", "8.6538").param("lon", "42"))
                .andExpect(status().isOk());

        // Hit
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", "8.6538").param("lon", "42"))
                .andExpect(status().isOk());

        // Hit
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", "Weather"))
                .andExpect(status().isOk());

        // Miss
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", "8.6538").param("lon", "42").param("api", "Weather"))
                .andExpect(status().isOk());

        // Hit
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", "8.6538").param("lon", "42"))
                .andExpect(status().isOk());

        mvc.perform(get("/api/cache/hitsnmisses").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.TotalRequests", is(totalReqs)))
                .andExpect(jsonPath("$.Hits", is(hits)))
                .andExpect(jsonPath("$.Misses", is(misses)));
    }


    @Test
    // Tests that when getting info by a registered locale, we get the info returned
    public void givenLocale_whenValidLocale_thenGetInfo_Current() throws Exception {
        String weather = "Weather";
        String breeze = "Breeze";

        this.createTestLocale("Aveiro", 8.6538, 40.6405);

        // For non-specified API
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data.*", hasSize(1)));

        // For Weather
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", weather))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.apiName", is("Weather")))
                .andExpect(jsonPath("$.data.0.aqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(1)));

        // For Breeze
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", breeze))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.apiName", is("Breeze")))
                .andExpect(jsonPath("$.data.0.baqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(1)));
    }


    @Test
    // Tests that when getting info by a non registered locale, we get an error
    public void givenLocale_whenInvalidLocale_thenGetError_Current() throws Exception {
        String invalidLocale = "Espinho";
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("locale", invalidLocale))
                .andExpect(status().isNotFound())
                .andExpect(status().reason(is("Error! Couldn't find any locale with the specified name")));

    }


    @Test
    // Tests that when given a valid API we get returned the info
    public void givenAPI_whenValidAPI_thenGetInfo_Current() throws Exception {
        String weather = "Weather";
        String breeze = "Breeze";

        this.createTestLocale("Aveiro", 8.6538, 40.6405);

        // For coords
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", "8.6538").param("lon", "40.6405").param("api", breeze))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Breeze")))
                .andExpect(jsonPath("$.data.0.baqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(1)));

        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", "8.6538").param("lon", "40.6405").param("api", weather))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Weather")))
                .andExpect(jsonPath("$.data.0.aqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(1)));

        // For locale
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", breeze))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Breeze")))
                .andExpect(jsonPath("$.data.0.baqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(1)));


        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", weather))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Weather")))
                .andExpect(jsonPath("$.data.0.aqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(1)));

    }


    @Test
    // Tests that when given an invalid API we get returned an error
    public void givenAPI_whenInvalidAPI_thenGetError_Current() throws Exception {
        String badAPIName = "Oof";

        this.createTestLocale("Aveiro", 8.6538, 40.6405);

        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", "8.6538").param("lon", "40.6405").param("api", badAPIName))
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason(is("Unexpected API value : " + badAPIName + " . Was expecting 'Breeze', 'Weather' or nothing")));

        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", badAPIName))
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason(is("Unexpected API value : " + badAPIName + " . Was expecting 'Breeze', 'Weather' or nothing")));
    }


    @Test
    // Tests that when the Breeze API returns an error, and no api is specified, we get the info from weatherbit
    public void givenNoAPI_whenValidCoord_thenGetBreeze_Current() throws Exception {
        // By testing we assertained that BreezeOMeter works for the following Coordinates
        double goodLon = 40;
        double goodLat = 8.6538;
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", "" + goodLon).param("lon", "" + goodLat))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Breeze")))
                .andExpect(jsonPath("$.data.0.baqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(1)));
    }


    @Test
    // Tests that when the Breeze API returns an error, and no api is specified, we get the info from weatherbit
    public void givenNoAPI_whenInvalidCoord_thenGetWeather_Current() throws Exception {
        // By testing we assertained that BreezeOMeter returns an error when given this lan/lon combo
        double badLon = 0;
        double badLat = 8.6538;
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", "" + badLat).param("lon", "" + badLon))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Weather")))
                .andExpect(jsonPath("$.data.0.aqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(1)));
    }


    @Test
    // Tests that when getting info by a registered locale, we get the info returned
    public void givenLocale_whenValidLocale_thenGetInfo_Past() throws Exception {
        String weather = "Weather";
        String breeze = "Breeze";

        this.createTestLocale("Aveiro", 8.6538, 40.6405);

        // For non-specified API
        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(3))));

        // For Weather
        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", weather))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.apiName", is("Weather")))
                .andExpect(jsonPath("$.data.0.aqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(72))));

        // For Breeze
        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", breeze))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.apiName", is("Breeze")))
                .andExpect(jsonPath("$.data.0.baqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(3))));
    }


    @Test
    // Tests that when getting info by a non registered locale, we get an error
    public void givenLocale_whenInvalidLocale_thenGetError_Past() throws Exception {
        String invalidLocale = "Espinho";
        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("locale", invalidLocale))
                .andExpect(status().isNotFound())
                .andExpect(status().reason(is("Error! Couldn't find any locale with the specified name")));

    }


    @Test
    // Tests that when given a valid API we get returned the info
    public void givenAPI_whenValidAPI_thenGetInfo_Past() throws Exception {
        String weather = "Weather";
        String breeze = "Breeze";

        this.createTestLocale("Aveiro", 8.6538, 40.6405);

        // For coords
        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("lat", "8.6538").param("lon", "40.6405").param("api", breeze))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Breeze")))
                .andExpect(jsonPath("$.data.0.baqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(3))));

        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("lat", "8.6538").param("lon", "40.6405").param("api", weather))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Weather")))
                .andExpect(jsonPath("$.data.0.aqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(72))));

        // For locale
        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", breeze))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Breeze")))
                .andExpect(jsonPath("$.data.0.baqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(3))));


        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", weather))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Weather")))
                .andExpect(jsonPath("$.data.0.aqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(72))));

    }


    @Test
    // Tests that when given an invalid API we get returned an error
    public void givenAPI_whenInvalidAPI_thenGetError_Past() throws Exception {
        String badAPIName = "Oof";

        this.createTestLocale("Aveiro", 8.6538, 40.6405);

        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("lat", "8.6538").param("lon", "40.6405").param("api", badAPIName))
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason(is("Unexpected API value : " + badAPIName + " . Was expecting 'Breeze', 'Weather' or nothing")));

        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", badAPIName))
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason(is("Unexpected API value : " + badAPIName + " . Was expecting 'Breeze', 'Weather' or nothing")));
    }


    @Test
    // Tests that when the Breeze API returns an error, and no api is specified, we get the info from weatherbit
    public void givenNoAPI_whenValidCoord_thenGetBreeze_Past() throws Exception {
        // By testing we assertained that BreezeOMeter works for the following Coordinates
        double goodLon = 40;
        double goodLat = 8.6538;
        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("lat", "" + goodLon).param("lon", "" + goodLat))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Breeze")))
                .andExpect(jsonPath("$.data.0.baqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(3))));
    }


    @Test
    // Tests that when the Breeze API returns an error, and no api is specified, we get the info from weatherbit
    public void givenNoAPI_whenInvalidCoord_thenGetWeather_Past() throws Exception {
        // By testing we assertained that BreezeOMeter returns an error when given this lan/lon combo
        double badLon = 0;
        double badLat = 8.6538;
        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("lat", "" + badLat).param("lon", "" + badLon))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Weather")))
                .andExpect(jsonPath("$.data.0.aqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(72))));
    }


    @Test
    // Tests that when getting info by a registered locale, we get the info returned
    public void givenLocale_whenValidLocale_thenGetInfo_Future() throws Exception {
        String weather = "Weather";
        String breeze = "Breeze";

        this.createTestLocale("Aveiro", 8.6538, 40.6405);

        // For non-specified API
        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(3))));

        // For Weather
        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", weather))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.apiName", is("Weather")))
                .andExpect(jsonPath("$.data.0.aqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(72))));

        // For Breeze
        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", breeze))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.apiName", is("Breeze")))
                .andExpect(jsonPath("$.data.0.baqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(3))));
    }


    @Test
    // Tests that when getting info by a non registered locale, we get an error
    public void givenLocale_whenInvalidLocale_thenGetError_Future() throws Exception {
        String invalidLocale = "Espinho";
        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("locale", invalidLocale))
                .andExpect(status().isNotFound())
                .andExpect(status().reason(is("Error! Couldn't find any locale with the specified name")));

    }


    @Test
    // Tests that when given a valid API we get returned the info
    public void givenAPI_whenValidAPI_thenGetInfo_Future() throws Exception {
        String weather = "Weather";
        String breeze = "Breeze";

        this.createTestLocale("Aveiro", 8.6538, 40.6405);

        // For coords
        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("lat", "8.6538").param("lon", "40.6405").param("api", breeze))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Breeze")))
                .andExpect(jsonPath("$.data.0.baqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(3))));

        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("lat", "8.6538").param("lon", "40.6405").param("api", weather))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Weather")))
                .andExpect(jsonPath("$.data.0.aqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(72))));

        // For locale
        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", breeze))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Breeze")))
                .andExpect(jsonPath("$.data.0.baqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(3))));


        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", weather))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Weather")))
                .andExpect(jsonPath("$.data.0.aqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(72))));

    }


    @Test
    // Tests that when given an invalid API we get returned an error
    public void givenAPI_whenInvalidAPI_thenGetError_Future() throws Exception {
        String badAPIName = "Oof";

        this.createTestLocale("Aveiro", 8.6538, 40.6405);

        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("lat", "8.6538").param("lon", "40.6405").param("api", badAPIName))
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason(is("Unexpected API value : " + badAPIName + " . Was expecting 'Breeze', 'Weather' or nothing")));

        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("locale", "Aveiro").param("api", badAPIName))
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason(is("Unexpected API value : " + badAPIName + " . Was expecting 'Breeze', 'Weather' or nothing")));
    }


    @Test
    // Tests that when the Breeze API returns an error, and no api is specified, we get the info from weatherbit
    public void givenNoAPI_whenValidCoord_thenGetBreeze_Future() throws Exception {
        // By testing we assertained that BreezeOMeter works for the following Coordinates
        double goodLon = 40;
        double goodLat = 8.6538;
        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("lat", "" + goodLon).param("lon", "" + goodLat))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Breeze")))
                .andExpect(jsonPath("$.data.0.baqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(3))));
    }


    @Test
    // Tests that when the Breeze API returns an error, and no api is specified, we get the info from weatherbit
    public void givenNoAPI_whenInvalidCoord_thenGetWeather_Future() throws Exception {
        // By testing we assertained that BreezeOMeter returns an error when given this lan/lon combo
        double badLon = 0;
        double badLat = 8.6538;
        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("lat", "" + badLat).param("lon", "" + badLon))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.apiName", is("Weather")))
                .andExpect(jsonPath("$.data.0.aqi").exists())
                .andExpect(jsonPath("$.data.*", hasSize(greaterThanOrEqualTo(72))));
    }

    // Support method used to insert a new Locale into the DB
    private void createTestLocale(String name, double lat, double lon) {
        Locale testLocale = new Locale(name, lat, lon);
        localeRepository.saveAndFlush(testLocale);
    }

    @Test
    public void givenNotNumbers_whenSearchingByCoords_returnBadRequestError() throws Exception {
        String lat = "oof";
        String lon = "oof";

        // Current
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON).param("lat", lat).param("lon", lon))
                .andExpect(status().isBadRequest());

        // Past
        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON).param("lat", lat).param("lon", lon))
                .andExpect(status().isBadRequest());

        // Future
        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON).param("lat", lat).param("lon", lon))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void givenNoParameters_whenSearchingForAirQuality_returnBadRequestError() throws Exception {
        // Current
        mvc.perform(get("/api/air").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason(containsString("Parameter conditions")))
                .andExpect(status().reason(containsString("not met for actual request parameters")));

        // Past
        mvc.perform(get("/api/air/past").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason(containsString("Parameter conditions")))
                .andExpect(status().reason(containsString("not met for actual request parameters")));


        // Future
        mvc.perform(get("/api/air/future").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason(containsString("Parameter conditions")))
                .andExpect(status().reason(containsString("not met for actual request parameters")));

    }

    @Test
    public void givenNoParameters_whenCreatingOrUpdatingLocale_returnBadRequestError() throws Exception {
        mvc.perform(put("/api/locale").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
        mvc.perform(post("/api/locale").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void givenNoParameters_whenDeletingLocale_returnBadRequestError() throws Exception {
        mvc.perform(delete("/api/locale").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason(is("Required String parameter 'locale' is not present")));
    }
}
