package tqs.air.aether.api;

import org.apache.commons.collections.map.HashedMap;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;

import org.springframework.lang.Nullable;
import tqs.air.aether.api.cache.LocalCache;
import tqs.air.aether.api.info.AirResponse;
import tqs.air.aether.api.info.Coordinates;
import tqs.air.aether.api.info.breeze.BreezeAirQuality;
import tqs.air.aether.api.info.weatherbit.WeatherAirQuality;
import tqs.air.aether.api.locale.Locale;
import tqs.air.aether.api.locale.LocalePOJO;
import tqs.air.aether.api.locale.LocaleRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase
public class AetherRestControllerTemplateIT {

    @LocalServerPort
    int randomServerPort;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private LocaleRepository localeRepository;

    @Autowired
    private LocalCache localCache;

    @AfterEach
    public void reset() {
        localeRepository.deleteAll();
        localCache.resetCache();
    }

    @Test
    // Tests that when creating a new, non-existent locale, it gets created
    public void whenValidInput_thenCreateLocale() throws Exception {
        LocalePOJO aveiro = new LocalePOJO("Aveiro", 8.6538, 41.6405);
        ResponseEntity<Locale> entity = restTemplate.postForEntity("/api/locale", aveiro, Locale.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        Locale foundLocale = localeRepository.findByName("Aveiro");
        assertThat(foundLocale.getName()).isEqualTo(aveiro.getName());
        assertThat(foundLocale.getLat()).isEqualTo(aveiro.getLat());
        assertThat(foundLocale.getLon()).isEqualTo(aveiro.getLon());

        List<Locale> found = localeRepository.findAll();
        assertThat(found).extracting(Locale::getName).containsOnly("Aveiro");
    }

    @Test
    // Tests that when creating an already existing locale, it returns an error
    public void whenInvalidInput_thenDoNotCreateLocale() throws Exception {
        double initialLon = 40.6405;
        double initialLat = 8.6538;
        this.createTestLocale("Espinho", initialLat, initialLon);

        LocalePOJO espinho = new LocalePOJO("Espinho", 8.6410, 41.0072);

        ResponseEntity<Exception> entity = restTemplate.postForEntity("/api/locale", espinho, Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);

        assertThat(entity.getBody().getMessage()).isEqualTo("Error! There already exists a locale named: " + espinho.getName());

        // Assure that the coordinates didn't get updated
        Locale foundLocale = localeRepository.findByName("Espinho");
        assertThat(foundLocale.getLat()).isEqualTo(initialLat);
        assertThat(foundLocale.getLon()).isEqualTo(initialLon);
    }

    @Test
    // Tests that when updating an existing locale it gets updated
    public void whenValidInput_thenUpdateLocale() throws Exception {
        this.createTestLocale("Espinho", 8.6538, 40.6405);

        double updatedLon = 41.0072;
        double updatedLat = 8.6410;
        LocalePOJO espinho = new LocalePOJO("Espinho", updatedLat, updatedLon);

        ResponseEntity<Locale> response = restTemplate
                .exchange("/api/locale", HttpMethod.PUT, new HttpEntity<LocalePOJO>(espinho, new HttpHeaders()), Locale.class);

        Locale foundLocale = localeRepository.findByName("Espinho");
        assertThat(foundLocale.getName()).isEqualTo(espinho.getName());
        assertThat(foundLocale.getLat()).isEqualTo(updatedLat);
        assertThat(foundLocale.getLon()).isEqualTo(updatedLon);
    }


    @Test
    // Tests that when updating a non existing locale, it returns an error
    public void whenInvalidInput_thenDoNotUpdateLocale() throws Exception {
        double updatedLon = 41.0072;
        double updatedLat = 8.6410;
        LocalePOJO espinho = new LocalePOJO("Espinho", updatedLat, updatedLon);

        ResponseEntity<Exception> entity = restTemplate
                .exchange("/api/locale", HttpMethod.PUT, new HttpEntity<LocalePOJO>(espinho, new HttpHeaders()), Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

        assertThat(entity.getBody().getMessage()).isEqualTo("Error! Couldn't find any locale named: " + espinho.getName());

        Locale foundLocale = localeRepository.findByName("Espinho");
        assertThat(foundLocale).isNull();
    }


    @Test
    // Tests that when given a valid locale, it deletes it
    public void whenValidInput_thenDeleteLocale() throws Exception {
        double lat = 8.6538;
        double lon = 40.6405;
        String name = "Espinho";

        this.createTestLocale(name, lat, lon);

        // Assert that we created the entity
        Locale foundLocale = localeRepository.findByName("Espinho");
        assertThat(foundLocale.getName()).isEqualTo(name);
        assertThat(foundLocale.getLat()).isEqualTo(lat);
        assertThat(foundLocale.getLon()).isEqualTo(lon);

        ResponseEntity<Locale> entity = restTemplate
                .exchange("/api/locale?locale=" + name, HttpMethod.DELETE, null, Locale.class);

        assertThat(entity.getBody().getName()).isEqualTo(name);

        // Assert that the entity was deleted
        foundLocale = localeRepository.findByName("Espinho");
        assertThat(foundLocale).isNull();
    }


    @Test
    // Tests that when given a non existent locale, it returns an error
    public void whenInvalidInput_thenDoNotDeleteLocale() throws Exception {
        double lat = 8.6538;
        double lon = 40.6405;
        String name = "Espinho";

        ResponseEntity<Exception> entity = restTemplate
                .exchange("/api/locale?locale=" + name, HttpMethod.DELETE, null, Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

        assertThat(entity.getBody().getMessage()).isEqualTo("Error! Couldn't find any locale with the specified name");
    }


    @Test
    // Tests that when fetching all locales, all locales get returned
    public void givenLocales_whenGetLocales_thenStatus200() throws Exception {
        this.createTestLocale("Aveiro", 8.6538, 40.6405);
        this.createTestLocale("Lisboa", 9.1393, 38.7223);
        this.createTestLocale("Porto", 8.6291, 41.1579);


        ResponseEntity<List<Locale>> response = restTemplate
                .exchange("/api/locale", HttpMethod.GET, null, new ParameterizedTypeReference<List<Locale>>() {
                });

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).extracting(Locale::getName).containsExactly("Aveiro", "Lisboa", "Porto");
    }


    @Test
    // Tests that when fetching cache we get returned the cache
    public void givenCacheItems_whenGetCache_thenStatus200() throws Exception {
        double lat = 8.6538;
        double lon = 40.6405;
        double lon2 = 42;
        String locale = "Aveiro";

        this.createTestLocale(locale, lat, lon);
        // Add to cache
        this.createTestCacheItem(lat,lon, locale, "Weather", "CURRENT");

        // Add to cache
        this.createTestCacheItem(lat,lon2,null ,null, "CURRENT");

        ResponseEntity<List<AirResponse>> response = restTemplate
                .exchange("/api/cache/items", HttpMethod.GET, null, new ParameterizedTypeReference<List<AirResponse>>() {
                });

        Coordinates coord1 = new Coordinates(lat, lon);
        coord1.setLocale("Aveiro");

        Coordinates coord2 = new Coordinates(lat, lon2);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).extracting(AirResponse::getCoordinates).containsExactly(coord1, coord2);
    }


    @Test
    // Tests that when fetching cache size we get the cache's size
    public void givenCacheSize_whenGetCacheSize_thenStatus200() throws Exception {
        this.createTestLocale("Aveiro", 8.6538, 40.6405);
        int expectedSize = 2;

        ResponseEntity<AirResponse> entity = restTemplate
                .exchange("/api/air?locale=Aveiro&api=Weather", HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);

        entity = restTemplate
                .exchange("/api/air?lat=8.6538&lon=42", HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);

        entity = restTemplate
                .exchange("/api/air?lat=8.6538&lon=42", HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);

        ResponseEntity<Integer> response = restTemplate
                .exchange("/api/cache/size", HttpMethod.GET, null, new ParameterizedTypeReference<Integer>() {
                });

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(expectedSize);
    }

    @Test
    // Tests that when fetching cache we get returned the cache
    public void givenCacheHitsNMisses_whenGetHitsNMisses_thenStatus200() throws Exception {
        int hits = 3;
        int misses = 3;
        int totalReqs = hits + misses;

        // Miss
        this.createTestLocale("Aveiro", 8.6538, 40.6405);

        ResponseEntity<AirResponse> entity = restTemplate
                .exchange("/api/air?locale=Aveiro&api=Weather", HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);

        // Miss
        entity = restTemplate
                .exchange("/api/air?lat=8.6538&lon=42", HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);

        // Hit
        entity = restTemplate
                .exchange("/api/air?lat=8.6538&lon=42", HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);

        // Hit
        entity = restTemplate
                .exchange("/api/air?locale=Aveiro&api=Weather", HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);

        // Miss
        entity = restTemplate
                .exchange("/api/air?lat=8.6538&lon=42&api=Weather", HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);

        // Hit
        entity = restTemplate
                .exchange("/api/air?lat=8.6538&lon=42", HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);

        ResponseEntity<Map<String, Integer>> response = restTemplate
                .exchange("/api/cache/hitsnmisses", HttpMethod.GET, null, new ParameterizedTypeReference<Map<String, Integer>>() {
                });

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().get("TotalRequests")).isEqualTo(totalReqs);
        assertThat(response.getBody().get("Hits")).isEqualTo(hits);
        assertThat(response.getBody().get("Misses")).isEqualTo(misses);
    }

    @Test
    // Tests that when getting info by a registered locale, we get the info returned
    public void givenLocale_whenValidLocale_thenGetInfo_Current() throws Exception {
        String weather = "Weather";
        String breeze = "Breeze";

        double lat = 8.6538;
        double lon = 40.6405;
        String locale = "Aveiro";

        this.createTestLocale(locale, lat, lon);

        // For non-specified API
        ResponseEntity<AirResponse> entity = restTemplate
                .exchange("/api/air?locale=" + locale, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getData().size()).isEqualTo(1);

        // For Weather
        entity = restTemplate
                .exchange("/api/air?locale=" + locale + "&api=" + weather, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(weather);
        assertThat(entity.getBody().getData().size()).isEqualTo(1);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(WeatherAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(BreezeAirQuality.class);

        // For Breeze
        entity = restTemplate
                .exchange("/api/air?locale=" + locale + "&api=" + breeze, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(breeze);
        assertThat(entity.getBody().getData().size()).isEqualTo(1);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(BreezeAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(WeatherAirQuality.class);
    }


    @Test
    // Tests that when getting info by a non registered locale, we get an error
    public void givenLocale_whenInvalidLocale_thenGetError_Current() throws Exception {
        String invalidLocale = "Espinho";

        ResponseEntity<Exception> entity = restTemplate
                .exchange("/api/air?locale=" + invalidLocale, HttpMethod.GET, null, Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(entity.getBody().getMessage()).isEqualTo("Error! Couldn't find any locale with the specified name");
    }


    @Test
    // Tests that when given a valid API we get returned the info
    public void givenAPI_whenValidAPI_thenGetInfo_Current() throws Exception {
        String weather = "Weather";
        String breeze = "Breeze";

        double lat = 8.6538;
        double lon = 40.6405;
        String locale = "Aveiro";

        this.createTestLocale(locale, lat, lon);

        // For coords
        ResponseEntity<AirResponse> entity = restTemplate
                .exchange("/api/air?lat=" + lat + "&lon=" + lon + "&api=" + breeze, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(breeze);
        assertThat(entity.getBody().getData().size()).isEqualTo(1);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(BreezeAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(WeatherAirQuality.class);

        entity = restTemplate
                .exchange("/api/air?lat=" + lat + "&lon=" + lon + "&api=" + weather, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(weather);
        assertThat(entity.getBody().getData().size()).isEqualTo(1);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(WeatherAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(BreezeAirQuality.class);

        // For locale
        entity = restTemplate
                .exchange("/api/air?locale=" + locale + "&api=" + breeze, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(breeze);
        assertThat(entity.getBody().getData().size()).isEqualTo(1);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(BreezeAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(WeatherAirQuality.class);

        entity = restTemplate
                .exchange("/api/air?locale=" + locale + "&api=" + weather, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(weather);
        assertThat(entity.getBody().getData().size()).isEqualTo(1);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(WeatherAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(BreezeAirQuality.class);

    }


    @Test
    // Tests that when given an invalid API we get returned an error
    public void givenAPI_whenInvalidAPI_thenGetError_Current() throws Exception {
        String badAPIName = "Oof";
        double lat = 8.6538;
        double lon = 40.6405;
        String locale = "Aveiro";

        this.createTestLocale(locale, lat, lon);

        // Locale
        ResponseEntity<Exception> entity = restTemplate
                .exchange("/api/air?locale=" + locale + "&api=" + badAPIName, HttpMethod.GET, null, Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(entity.getBody().getMessage()).isEqualTo("Unexpected API value : " + badAPIName + " . Was expecting 'Breeze', 'Weather' or nothing");

        // Coords
        entity = restTemplate
                .exchange("/api/air?lat=" + lat + "&lon=" + lon + "&api=" + badAPIName, HttpMethod.GET, null, Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(entity.getBody().getMessage()).isEqualTo("Unexpected API value : " + badAPIName + " . Was expecting 'Breeze', 'Weather' or nothing");
    }


    @Test
    // Tests that when the Breeze API returns an error, and no api is specified, we get the info from weatherbit
    public void givenNoAPI_whenValidCoord_thenGetBreeze_Current() throws Exception {
        // By testing we assertained that BreezeOMeter works for the following Coordinates
        double goodLon = 40;
        double goodLat = 8.6538;
        ResponseEntity<AirResponse> entity = restTemplate
                .exchange("/api/air?lat=" + goodLat + "&lon=" + goodLon, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo("Breeze");
        assertThat(entity.getBody().getData().size()).isEqualTo(1);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(BreezeAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(WeatherAirQuality.class);
    }

    @Test
    // Tests that when the Breeze API returns an error, and no api is specified, we get the info from weatherbit
    public void givenNoAPI_whenInvalidCoord_thenGetWeather_Current() throws Exception {
        // By testing we assertained that BreezeOMeter returns an error when given this lan/lon combo
        double badLon = 0;
        double badLat = 8.6538;

        ResponseEntity<AirResponse> entity = restTemplate
                .exchange("/api/air?lat=" + badLon + "&lon=" + badLat, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo("Weather");
        assertThat(entity.getBody().getData().size()).isEqualTo(1);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(WeatherAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(BreezeAirQuality.class);
    }

    @Test
    // Tests that when getting info by a registered locale, we get the info returned
    public void givenLocale_whenValidLocale_thenGetInfo_Past() throws Exception {
        String weather = "Weather";
        String breeze = "Breeze";

        double lat = 8.6538;
        double lon = 40.6405;
        String locale = "Aveiro";

        this.createTestLocale(locale, lat, lon);

        // For non-specified API
        ResponseEntity<AirResponse> entity = restTemplate
                .exchange("/api/air/past?locale=" + locale, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(3);

        // For Weather
        entity = restTemplate
                .exchange("/api/air/past?locale=" + locale + "&api=" + weather, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(weather);
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(72);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(WeatherAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(BreezeAirQuality.class);

        // For Breeze
        entity = restTemplate
                .exchange("/api/air/past?locale=" + locale + "&api=" + breeze, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(breeze);
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(3);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(BreezeAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(WeatherAirQuality.class);
    }


    @Test
    // Tests that when getting info by a non registered locale, we get an error
    public void givenLocale_whenInvalidLocale_thenGetError_Past() throws Exception {
        String invalidLocale = "Espinho";

        ResponseEntity<Exception> entity = restTemplate
                .exchange("/api/air/past?locale=" + invalidLocale, HttpMethod.GET, null, Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(entity.getBody().getMessage()).isEqualTo("Error! Couldn't find any locale with the specified name");
    }


    @Test
    // Tests that when given a valid API we get returned the info
    public void givenAPI_whenValidAPI_thenGetInfo_Past() throws Exception {
        String weather = "Weather";
        String breeze = "Breeze";

        double lat = 8.6538;
        double lon = 40.6405;
        String locale = "Aveiro";

        this.createTestLocale(locale, lat, lon);

        // For coords
        ResponseEntity<AirResponse> entity = restTemplate
                .exchange("/api/air/past?lat=" + lat + "&lon=" + lon + "&api=" + breeze, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(breeze);
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(3);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(BreezeAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(WeatherAirQuality.class);

        entity = restTemplate
                .exchange("/api/air/past?lat=" + lat + "&lon=" + lon + "&api=" + weather, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(weather);
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(72);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(WeatherAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(BreezeAirQuality.class);

        // For locale
        entity = restTemplate
                .exchange("/api/air/past?locale=" + locale + "&api=" + breeze, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(breeze);
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(3);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(BreezeAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(WeatherAirQuality.class);

        entity = restTemplate
                .exchange("/api/air/past?locale=" + locale + "&api=" + weather, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(weather);
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(72);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(WeatherAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(BreezeAirQuality.class);

    }


    @Test
    // Tests that when given an invalid API we get returned an error
    public void givenAPI_whenInvalidAPI_thenGetError_Past() throws Exception {
        String badAPIName = "Oof";
        double lat = 8.6538;
        double lon = 40.6405;
        String locale = "Aveiro";

        this.createTestLocale(locale, lat, lon);

        // Locale
        ResponseEntity<Exception> entity = restTemplate
                .exchange("/api/air/past?locale=" + locale + "&api=" + badAPIName, HttpMethod.GET, null, Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(entity.getBody().getMessage()).isEqualTo("Unexpected API value : " + badAPIName + " . Was expecting 'Breeze', 'Weather' or nothing");

        // Coords
        entity = restTemplate
                .exchange("/api/air/past?lat=" + lat + "&lon=" + lon + "&api=" + badAPIName, HttpMethod.GET, null, Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(entity.getBody().getMessage()).isEqualTo("Unexpected API value : " + badAPIName + " . Was expecting 'Breeze', 'Weather' or nothing");
    }


    @Test
    // Tests that when the Breeze API returns an error, and no api is specified, we get the info from weatherbit
    public void givenNoAPI_whenValidCoord_thenGetBreeze_Past() throws Exception {
        // By testing we assertained that BreezeOMeter works for the following Coordinates
        double goodLon = 40;
        double goodLat = 8.6538;
        ResponseEntity<AirResponse> entity = restTemplate
                .exchange("/api/air/past?lat=" + goodLat + "&lon=" + goodLon, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo("Breeze");
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(3);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(BreezeAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(WeatherAirQuality.class);
    }

    @Test
    // Tests that when the Breeze API returns an error, and no api is specified, we get the info from weatherbit
    public void givenNoAPI_whenInvalidCoord_thenGetWeather_Past() throws Exception {
        // By testing we assertained that BreezeOMeter returns an error when given this lan/lon combo
        double badLon = 0;
        double badLat = 8.6538;

        ResponseEntity<AirResponse> entity = restTemplate
                .exchange("/api/air/past?lat=" + badLon + "&lon=" + badLat, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo("Weather");
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(72);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(WeatherAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(BreezeAirQuality.class);
    }

    @Test
    // Tests that when getting info by a registered locale, we get the info returned
    public void givenLocale_whenValidLocale_thenGetInfo_Future() throws Exception {
        String weather = "Weather";
        String breeze = "Breeze";

        double lat = 8.6538;
        double lon = 40.6405;
        String locale = "Aveiro";

        this.createTestLocale(locale, lat, lon);

        // For non-specified API
        ResponseEntity<AirResponse> entity = restTemplate
                .exchange("/api/air/future?locale=" + locale, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(3);

        // For Weather
        entity = restTemplate
                .exchange("/api/air/future?locale=" + locale + "&api=" + weather, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(weather);
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(72);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(WeatherAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(BreezeAirQuality.class);

        // For Breeze
        entity = restTemplate
                .exchange("/api/air/future?locale=" + locale + "&api=" + breeze, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(breeze);
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(3);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(BreezeAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(WeatherAirQuality.class);
    }


    @Test
    // Tests that when getting info by a non registered locale, we get an error
    public void givenLocale_whenInvalidLocale_thenGetError_Future() throws Exception {
        String invalidLocale = "Espinho";

        ResponseEntity<Exception> entity = restTemplate
                .exchange("/api/air/future?locale=" + invalidLocale, HttpMethod.GET, null, Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(entity.getBody().getMessage()).isEqualTo("Error! Couldn't find any locale with the specified name");
    }


    @Test
    // Tests that when given a valid API we get returned the info
    public void givenAPI_whenValidAPI_thenGetInfo_Future() throws Exception {
        String weather = "Weather";
        String breeze = "Breeze";

        double lat = 8.6538;
        double lon = 40.6405;
        String locale = "Aveiro";

        this.createTestLocale(locale, lat, lon);

        // For coords
        ResponseEntity<AirResponse> entity = restTemplate
                .exchange("/api/air/future?lat=" + lat + "&lon=" + lon + "&api=" + breeze, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(breeze);
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(3);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(BreezeAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(WeatherAirQuality.class);

        entity = restTemplate
                .exchange("/api/air/future?lat=" + lat + "&lon=" + lon + "&api=" + weather, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(weather);
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(72);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(WeatherAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(BreezeAirQuality.class);

        // For locale
        entity = restTemplate
                .exchange("/api/air/future?locale=" + locale + "&api=" + breeze, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(breeze);
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(3);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(BreezeAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(WeatherAirQuality.class);

        entity = restTemplate
                .exchange("/api/air/future?locale=" + locale + "&api=" + weather, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo(weather);
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(72);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(WeatherAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(BreezeAirQuality.class);

    }


    @Test
    // Tests that when given an invalid API we get returned an error
    public void givenAPI_whenInvalidAPI_thenGetError_Future() throws Exception {
        String badAPIName = "Oof";
        double lat = 8.6538;
        double lon = 40.6405;
        String locale = "Aveiro";

        this.createTestLocale(locale, lat, lon);

        // Locale
        ResponseEntity<Exception> entity = restTemplate
                .exchange("/api/air/future?locale=" + locale + "&api=" + badAPIName, HttpMethod.GET, null, Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(entity.getBody().getMessage()).isEqualTo("Unexpected API value : " + badAPIName + " . Was expecting 'Breeze', 'Weather' or nothing");

        // Coords
        entity = restTemplate
                .exchange("/api/air/future?lat=" + lat + "&lon=" + lon + "&api=" + badAPIName, HttpMethod.GET, null, Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(entity.getBody().getMessage()).isEqualTo("Unexpected API value : " + badAPIName + " . Was expecting 'Breeze', 'Weather' or nothing");
    }


    @Test
    // Tests that when the Breeze API returns an error, and no api is specified, we get the info from weatherbit
    public void givenNoAPI_whenValidCoord_thenGetBreeze_Future() throws Exception {
        // By testing we assertained that BreezeOMeter works for the following Coordinates
        double goodLon = 40;
        double goodLat = 8.6538;
        ResponseEntity<AirResponse> entity = restTemplate
                .exchange("/api/air/future?lat=" + goodLat + "&lon=" + goodLon, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo("Breeze");
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(3);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(BreezeAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(WeatherAirQuality.class);
    }

    @Test
    // Tests that when the Breeze API returns an error, and no api is specified, we get the info from weatherbit
    public void givenNoAPI_whenInvalidCoord_thenGetWeather_Future() throws Exception {
        // By testing we assertained that BreezeOMeter returns an error when given this lan/lon combo
        double badLon = 0;
        double badLat = 8.6538;

        ResponseEntity<AirResponse> entity = restTemplate
                .exchange("/api/air/future?lat=" + badLon + "&lon=" + badLat, HttpMethod.GET, null, AirResponse.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody().getApiName()).isEqualTo("Weather");
        assertThat(entity.getBody().getData().size()).isGreaterThanOrEqualTo(72);
        assertThat(entity.getBody().getData().get(0)).isInstanceOf(WeatherAirQuality.class);
        assertThat(entity.getBody().getData().get(0)).isNotInstanceOf(BreezeAirQuality.class);
    }

    @Test
    public void givenNotNumbers_whenSearchingByCoords_returnBadRequestError() throws Exception {
        String lat = "oof";
        String lon = "oof";

        ResponseEntity<Exception> entity = restTemplate
                .exchange("/api/air?lat=" + lat + "&lon=" + lon, HttpMethod.GET, null, Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        entity = restTemplate
                .exchange("/api/air/past?lat=" + lat + "&lon=" + lon, HttpMethod.GET, null, Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        entity = restTemplate
                .exchange("/api/air/future?lat=" + lat + "&lon=" + lon, HttpMethod.GET, null, Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void givenNoParameters_whenSearchingForAirQuality_returnBadRequestError() throws Exception {
        ResponseEntity<Exception> entity = restTemplate
                .exchange("/api/air", HttpMethod.GET, null, Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(entity.getBody().getMessage()).contains("Parameter conditions");
        assertThat(entity.getBody().getMessage()).contains("not met for actual request parameters");

        entity = restTemplate
                .exchange("/api/air/past", HttpMethod.GET, null, Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(entity.getBody().getMessage()).contains("Parameter conditions");
        assertThat(entity.getBody().getMessage()).contains("not met for actual request parameters");

        entity = restTemplate
                .exchange("/api/air/future", HttpMethod.GET, null, Exception.class);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(entity.getBody().getMessage()).contains("Parameter conditions");
        assertThat(entity.getBody().getMessage()).contains("not met for actual request parameters");
    }

    @Test
    public void givenNoParameters_whenCreatingOrUpdatingLocale_returnBadRequestError() throws Exception {
        ResponseEntity<Exception> entity = restTemplate
                .exchange("/api/locale", HttpMethod.PUT, null, Exception.class);
        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        entity = restTemplate
                .exchange("/api/locale", HttpMethod.POST, null, Exception.class);
        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void givenNoParameters_whenDeletingLocale_returnBadRequestError() throws Exception {
        ResponseEntity<Exception> entity = restTemplate
                .exchange("/api/locale", HttpMethod.DELETE, null, Exception.class);
        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(entity.getBody().getMessage()).isEqualTo("Required String parameter 'locale' is not present");
    }

    // Support method used to insert a new Locale into the DB
    private void createTestLocale(String name, double lat, double lon) {
        Locale testLocale = new Locale(name, lat, lon);
        localeRepository.saveAndFlush(testLocale);
    }

    // Support method used to insert items into our cache
    private void createTestCacheItem(double lat, double lon, @Nullable String locale, String apiName, String reqType) {
        AirResponse testResponse = new AirResponse();
        Coordinates testCoordinates = new Coordinates(lat,lon);
        testCoordinates.setLocale(locale);

        testResponse.setCoordinates(testCoordinates);
        testResponse.setRequestDate(LocalDateTime.now());
        testResponse.setApiName(apiName);
        localCache.addToCache(testResponse, reqType);
    }
}