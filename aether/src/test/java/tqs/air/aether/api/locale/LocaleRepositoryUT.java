package tqs.air.aether.api.locale;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class LocaleRepositoryUT {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private LocaleRepository localeRepository;

    @Test
    public void whenFindByName_thenReturnLocale() {
        Locale aveiro = new Locale("Aveiro", 8.6538, 40.6405);
        entityManager.persistAndFlush(aveiro);

        Locale found = localeRepository.findByName(aveiro.getName());
        assertThat(found.getName()).isEqualTo(aveiro.getName());
    }

    @Test
    public void whenInvalidName_thenReturnNull() {
        Locale found = localeRepository.findByName("Invalid Locale");
        assertThat(found).isNull();
    }

    @Test
    public void givenSetOfLocales_whenFindAll_thenReturnAllLocales() {
        Locale aveiro = new Locale("Aveiro", 8.6538, 40.6405);
        Locale porto = new Locale("Porto", 8.6291, 41.1579);
        Locale lisboa = new Locale("Lisboa", 9.1393, 38.7223);


        entityManager.persist(aveiro);
        entityManager.persist(porto);
        entityManager.persist(lisboa);
        entityManager.flush();

        List<Locale> allEmployees = localeRepository.findAll();

        assertThat(allEmployees).hasSize(3).extracting(Locale::getName).containsOnly(aveiro.getName(), porto.getName(), lisboa.getName());
        assertThat(allEmployees).extracting(Locale::getLat).containsOnly(aveiro.getLat(), porto.getLat(), lisboa.getLat());
        assertThat(allEmployees).extracting(Locale::getLon).containsOnly(aveiro.getLon(), porto.getLon(), lisboa.getLon());
    }

    @Test
    public void givenNoLocales_whenFindAll_thenReturnEmptyList() {
        List<Locale> allEmployees = localeRepository.findAll();

        assertThat(allEmployees).hasSize(0);
    }

}