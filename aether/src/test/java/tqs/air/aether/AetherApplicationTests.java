package tqs.air.aether;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import tqs.air.aether.api.AetherRestController;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class AetherApplicationTests {
    @Autowired
    private AetherRestController controller;

    @Test
    void contextLoads() {
        assertThat(controller).isNotNull();
    }

}
