package functional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import tqs.air.aether.AetherApplication;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes= AetherApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InterfaceTests {
    PageObject controller;

    @LocalServerPort
    private int port;

    @BeforeEach
    public void setUp() {
        controller = new PageObject();
    }

    @AfterEach
    public void tearDown() {
        controller.tear();
    }

    @Test
    public void whenInvalidCoord_thenShowError() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");


        controller.searchByCoordinates("bid", "Oof", null, null);
        assertTrue(controller.checkVisibility("error2"));


        controller.searchByCoordinates("", null, null, null);
        assertTrue(controller.checkVisibility("error2"));


        controller.searchByCoordinates("bid", "", "WeatherBit", null);
        assertTrue(controller.checkVisibility("error2"));


        controller.searchByCoordinates("", "", "BreezeOMeter", "Future forecast");
        assertTrue(controller.checkVisibility("error2"));
    }

    @Test
    public void whenInvalidInput_whenCreatingNewLocale_thenShowError() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        controller.addNewLocale(null, null, null);
        assertTrue(controller.checkVisibility("errorNew"));

        controller.addNewLocale("Oof", null, null);
        assertTrue(controller.checkVisibility("errorNew"));

        controller.addNewLocale(null, "Oof", null);
        assertTrue(controller.checkVisibility("errorNew"));

        controller.addNewLocale(null, null, "Oof");
        assertTrue(controller.checkVisibility("errorNew"));

        controller.addNewLocale(null, "", null);
        assertTrue(controller.checkVisibility("errorNew"));

        controller.addNewLocale(null, null, "");
        assertTrue(controller.checkVisibility("errorNew"));

        controller.addNewLocale("", "4", "20");
        assertTrue(controller.checkVisibility("errorNew"));
    }

    @Test
    public void whenInvalidInput_whenUpdatingNewLocale_thenShowError() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        controller.updateLocale(null, null, null);
        assertTrue(controller.checkVisibility("errorUpdate"));

        controller.updateLocale("Oof", null, null);
        assertTrue(controller.checkVisibility("errorUpdate"));

        controller.updateLocale(null, "Oof", null);
        assertTrue(controller.checkVisibility("errorUpdate"));

        controller.updateLocale(null, null, "Oof");
        assertTrue(controller.checkVisibility("errorUpdate"));

        controller.updateLocale(null, "", null);
        assertTrue(controller.checkVisibility("errorUpdate"));

        controller.updateLocale(null, null, "");
        assertTrue(controller.checkVisibility("errorUpdate"));

        controller.updateLocale("", "4", "20");
        assertTrue(controller.checkVisibility("errorUpdate"));
    }

    @Test
    public void whenInvalidInput_whenDeletingLocale_thenShowError() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        controller.deleteLocale(null);
        assertTrue(controller.checkVisibility("deleteError"));
    }

    @Test
    public void whenValidCoord_afterInvalidCoord_thenCleanError() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // Cause Error
        controller.searchByCoordinates("bid", "Oof", null, null);
        assertTrue(controller.checkVisibility("error2"));

        // Correct query
        controller.searchByCoordinates("8", "40", null, null);
        assertFalse(controller.checkVisibility("error2"));
    }

    @Test
    public void whenLocale_afterInvalidLocale_thenCleanError() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // Delete locale, if need be
        controller.deleteLocale("Oof");
        assertFalse(controller.checkVisibility("deleteError"));

        // Cause Error
        controller.searchByLocale(null, "Either API", "Current");
        assertTrue(controller.checkVisibility("error3"));

        // Add Locale
        controller.addNewLocale("Oof", "5", "5");
        assertFalse(controller.checkVisibility("errorNew"));

        // Correct query
        controller.searchByLocale("Oof (5, 5)", "Either API", "Current");
        assertFalse(controller.checkVisibility("error3"));
    }

    @Test
    public void whenValidInput_afterInvalidInput_whenCreatingLocale_thenCleanError() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // Cause Error
        controller.addNewLocale("", "5", "5");
        assertTrue(controller.checkVisibility("errorNew"));

        // Correct query
        controller.addNewLocale("Oof", "5", "5");
        assertFalse(controller.checkVisibility("errorNew"));
    }

    @Test
    public void whenValidInput_afterInvalidInput_whenUpdatingLocale_thenCleanError() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // Cause Error
        controller.updateLocale("", "5", "5");
        assertTrue(controller.checkVisibility("errorUpdate"));

        // Correct query
        controller.updateLocale("Oof", "5", "5");
        assertFalse(controller.checkVisibility("errorUpdate"));
    }

    @Test
    public void whenValidInput_afterInvalidInput_whenDeletingLocale_thenCleanError() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // Cause Error
        controller.deleteLocale("");
        assertTrue(controller.checkVisibility("deleteError"));

        // Correct query
        controller.deleteLocale("Oof");
        assertFalse(controller.checkVisibility("deleteError"));
    }

    @Test
    public void whenValidCoord_thenGetInfo_Current() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // Current
        controller.searchByCoordinates("8", "15", "Either API", null);
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        assertTrue(controller.checkVisibility("searchDiv2"));

        // Weather
        controller.searchByCoordinates("50", "15", "WeatherBit", null);
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        // Verify we loaded either an error, or a result with the WeatherBit title
        assertTrue(controller.checkVisibility("searchDiv2"));
        assertTrue(controller.checkText("search2Api", "WeatherBit Results"));

        // Breeze
        controller.searchByCoordinates("8", "40", "BreezeOMeter", null);
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        // Verify we loaded a result with the BreezeOMeter title
        assertTrue(controller.checkVisibility("searchDiv2"));
        assertTrue(controller.checkText("search2Api", "BreezeOMeter Results"));
    }

    @Test
    public void whenValidCoord_thenGetInfo_Past() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // no api
        controller.searchByCoordinates("8", "15", "Either API", "Past history");
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        assertTrue(controller.checkVisibility("searchDiv2"));
        assertTrue(controller.checkVisibility("search2ReqPF"));
        assertThat(controller.getNumberOfSelectOptions("search2ReqPF")).isGreaterThanOrEqualTo(3);

        // weather
        controller.searchByCoordinates("50", "15", "WeatherBit", "Past history");
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        // Verify we loaded either an error, or a result with the WeatherBit title
        assertTrue(controller.checkVisibility("searchDiv2"));
        assertTrue(controller.checkText("search2Api", "WeatherBit Results"));
        assertTrue(controller.checkVisibility("search2ReqPF"));
        assertThat(controller.getNumberOfSelectOptions("search2ReqPF")).isGreaterThanOrEqualTo(3);

        // breeze
        controller.searchByCoordinates("8", "40", "BreezeOMeter", "Past history");
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        // Verify we loaded a result with the BreezeOMeter title
        assertTrue(controller.checkVisibility("searchDiv2"));
        assertTrue(controller.checkText("search2Api", "BreezeOMeter Results"));
        assertTrue(controller.checkVisibility("search2ReqPF"));
        assertThat(controller.getNumberOfSelectOptions("search2ReqPF")).isGreaterThanOrEqualTo(3);
    }

    @Test
    public void whenValidCoord_thenGetInfo_Future() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // no api
        controller.searchByCoordinates("5", "115", "Either API", "Future forecast");
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        assertTrue(controller.checkVisibility("searchDiv2"));
        assertTrue(controller.checkVisibility("search2ReqPF"));
        assertThat(controller.getNumberOfSelectOptions("search2ReqPF")).isGreaterThanOrEqualTo(3);

        // weather
        controller.searchByCoordinates("12", "15", "WeatherBit", "Future forecast");
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        // Verify we loaded either an error, or a result with the WeatherBit title
        assertTrue(controller.checkVisibility("searchDiv2"));
        assertTrue(controller.checkText("search2Api", "WeatherBit Results"));
        assertTrue(controller.checkVisibility("search2ReqPF"));
        assertThat(controller.getNumberOfSelectOptions("search2ReqPF")).isGreaterThanOrEqualTo(3);

        // breeze
        controller.searchByCoordinates("8", "40", "BreezeOMeter", "Future forecast");
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        // Verify we loaded a result with the BreezeOMeter title
        assertTrue(controller.checkVisibility("searchDiv2"));
        assertTrue(controller.checkText("search2Api", "BreezeOMeter Results"));
        assertTrue(controller.checkVisibility("search2ReqPF"));
        assertThat(controller.getNumberOfSelectOptions("search2ReqPF")).isGreaterThanOrEqualTo(3);
    }

    @Test
    public void whenValidLocale_thenGetInfo_Current() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // In case it's not already created, create a valid locale
        controller.deleteLocale("Oof");
        assertFalse(controller.checkVisibility("deleteError"));

        controller.addNewLocale("Oof", "8", "40");
        assertFalse(controller.checkVisibility("errorNew"));

        // No API
        controller.searchByLocale("Oof (8, 40)", "Either API", null);
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        assertTrue(controller.checkVisibility("searchDiv2"));

        // WeatherBit
        controller.searchByLocale("Oof (8, 40)", "WeatherBit", null);
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        // Verify we loaded either an error, or a result with the WeatherBit title
        assertTrue(controller.checkVisibility("searchDiv2"));
        assertTrue(controller.checkText("search2Api", "WeatherBit Results"));

        // BreezeOMeter
        controller.searchByLocale("Oof (8, 40)", "BreezeOMeter", null);
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        // Verify we loaded a result with the BreezeOMeter title
        assertTrue(controller.checkVisibility("searchDiv2"));
        assertTrue(controller.checkText("search2Api", "BreezeOMeter Results"));
    }

    @Test
    public void whenValidLocale_thenGetInfo_Past() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // In case it's not already created, create a valid locale
        controller.deleteLocale("Oof");
        assertFalse(controller.checkVisibility("deleteError"));

        controller.addNewLocale("Oof", "8", "40");
        assertFalse(controller.checkVisibility("errorNew"));

        // No API
        controller.searchByLocale("Oof (8, 40)", "Either API", "Past history");
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        assertTrue(controller.checkVisibility("searchDiv2"));
        assertTrue(controller.checkVisibility("search2ReqPF"));
        assertThat(controller.getNumberOfSelectOptions("search2ReqPF")).isGreaterThanOrEqualTo(3);

        // WeatherBit
        controller.searchByLocale("Oof (8, 40)", "WeatherBit", "Past history");
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        // Verify we loaded either an error, or a result with the WeatherBit title
        assertTrue(controller.checkVisibility("searchDiv2"));
        assertTrue(controller.checkText("search2Api", "WeatherBit Results"));
        assertTrue(controller.checkVisibility("search2ReqPF"));
        assertThat(controller.getNumberOfSelectOptions("search2ReqPF")).isGreaterThanOrEqualTo(3);

        // Breeze
        controller.searchByLocale("Oof (8, 40)", "BreezeOMeter", "Past history");
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        // Verify we loaded a result with the BreezeOMeter title
        assertTrue(controller.checkVisibility("searchDiv2"));
        assertTrue(controller.checkText("search2Api", "BreezeOMeter Results"));
        assertTrue(controller.checkVisibility("search2ReqPF"));
        assertThat(controller.getNumberOfSelectOptions("search2ReqPF")).isGreaterThanOrEqualTo(3);
    }

    @Test
    public void whenValidLocale_thenGetInfo_Future() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // In case it's not already created, create a valid locale
        controller.deleteLocale("Oof");
        assertFalse(controller.checkVisibility("deleteError"));

        controller.addNewLocale("Oof", "8", "40");
        assertFalse(controller.checkVisibility("errorNew"));

        // No API
        controller.searchByLocale("Oof (8, 40)", "Either API", "Future forecast");
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        assertTrue(controller.checkVisibility("searchDiv2"));
        assertTrue(controller.checkVisibility("search2ReqPF"));
        assertThat(controller.getNumberOfSelectOptions("search2ReqPF")).isGreaterThanOrEqualTo(3);

        // WeatherBit
        controller.searchByLocale("Oof (8, 40)", "WeatherBit", "Future forecast");
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        // Verify we loaded either an error, or a result with the WeatherBit title
        assertTrue(controller.checkVisibility("searchDiv2"));
        assertTrue(controller.checkText("search2Api", "WeatherBit Results"));
        assertTrue(controller.checkVisibility("search2ReqPF"));
        assertThat(controller.getNumberOfSelectOptions("search2ReqPF")).isGreaterThanOrEqualTo(3);

        // Breeze
        controller.searchByLocale("Oof (8, 40)", "BreezeOMeter", "Future forecast");
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        // Verify we loaded a result with the BreezeOMeter title
        assertTrue(controller.checkVisibility("searchDiv2"));
        assertTrue(controller.checkText("search2Api", "BreezeOMeter Results"));
        assertTrue(controller.checkVisibility("search2ReqPF"));
        assertThat(controller.getNumberOfSelectOptions("search2ReqPF")).isGreaterThanOrEqualTo(3);
    }

    @Test
    public void whenBadCoord_thenGetError() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // no api
        controller.searchByCoordinates("0", "0", "BreezeOMeter", null);
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        assertTrue(controller.checkVisibility("errorDiv2"));

        // Weather
        controller.searchByCoordinates("0", "0", "BreezeOMeter", "Past history");
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        assertTrue(controller.checkVisibility("errorDiv2"));

        // Breeze
        controller.searchByCoordinates("0", "0", "BreezeOMeter", "Future forecast");
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        assertTrue(controller.checkVisibility("errorDiv2"));
    }

    @Test
    public void whenBadLocalethenGetError() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // Just in case make sure the locale exists as intended
        controller.deleteLocale("Oof");
        assertFalse(controller.checkVisibility("deleteError"));

        controller.addNewLocale("Oof", "0", "0");
        assertFalse(controller.checkVisibility("errorNew"));

        // no api
        controller.searchByLocale("Oof (0, 0)", "BreezeOMeter", null);
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        assertTrue(controller.checkVisibility("errorDiv2"));

        // Weather
        controller.searchByLocale("Oof (0, 0)", "BreezeOMeter", "Past history");
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        assertTrue(controller.checkVisibility("errorDiv2"));

        // Breeze
        controller.searchByLocale("Oof (0, 0)", "BreezeOMeter", "Future forecast");
        assertFalse(controller.checkVisibility("error2"));
        // Wait until its done loading
        controller.waitForLoad("postloader2");
        assertTrue(controller.checkVisibility("errorDiv2"));
    }

    @Test
    public void whenValidInput_whenCreatingNewLocale_andNonExistantLocale_showSuccess() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // Just in case make sure the locale doesn't exist
        controller.deleteLocale("Oof");
        assertFalse(controller.checkVisibility("deleteError"));

        controller.addNewLocale("Oof", "5", "5");
        assertFalse(controller.checkVisibility("errorNew"));
        // Wait until its done loading
        controller.waitForLoad("postloader4");
        assertTrue(controller.checkVisibility("success"));
        assertTrue(controller.checkText("succType", "Created:"));
    }

    @Test
    public void whenValidInput_whenCreatingNewLocale_andExistantLocale_showError() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // Just in case make sure the locale doesn't exist
        controller.addNewLocale("Oof", "5", "5");
        assertFalse(controller.checkVisibility("errorNew"));

        controller.addNewLocale("Oof", "5", "5");
        assertFalse(controller.checkVisibility("errorNew"));
        // Wait until its done loading
        controller.waitForLoad("postloader4");
        assertTrue(controller.checkVisibility("errorDiv4"));
    }

    @Test
    public void whenValidInput_whenUpdatingLocale_andExistantLocale_showSuccess() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // Just in case make sure the locale exists
        controller.addNewLocale("Oof", "5", "5");
        assertFalse(controller.checkVisibility("errorNew"));

        controller.updateLocale("Oof", "10", "10");
        assertFalse(controller.checkVisibility("errorUpdate"));
        // Wait until its done loading
        controller.waitForLoad("postloader4");
        assertTrue(controller.checkVisibility("success"));
        assertTrue(controller.checkText("succType", "Updated:"));
    }

    @Test
    public void whenValidInput_whenUpdatingLocale_andNonExistantLocale_showError() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // Just in case make sure the locale doesn't exist
        controller.deleteLocale("Oof");
        assertFalse(controller.checkVisibility("deleteError"));

        controller.updateLocale("Oof", "10", "10");
        assertFalse(controller.checkVisibility("errorUpdate"));
        // Wait until its done loading
        controller.waitForLoad("postloader4");
        assertTrue(controller.checkVisibility("errorDiv4"));
    }

    @Test
    public void whenValidInput_whenDeletingLocale_andExistantLocale_showSuccess() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // Just in case make sure the locale exists
        controller.addNewLocale("Oof", "5", "5");
        assertFalse(controller.checkVisibility("errorNew"));

        controller.deleteLocale("Oof");
        assertFalse(controller.checkVisibility("deleteError"));
        // Wait until its done loading
        controller.waitForLoad("postloader4");
        assertTrue(controller.checkVisibility("success"));
        assertTrue(controller.checkText("succType", "Deleted:"));
    }

    @Test
    public void whenValidInput_whenDeletingLocale_andNonExistantLocale_showError() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // Just in case make sure the locale doesn't exist
        controller.deleteLocale("Oof");
        assertFalse(controller.checkVisibility("deleteError"));

        // Wait until its done loading
        controller.deleteLocale("Oof");
        assertFalse(controller.checkVisibility("deleteError"));

    }

    @Test
    public void whenClickCache_getCacheInfo() {
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        controller.getCacheInfo();

        // Wait until its done loading
        controller.waitForLoad("postloader5");
        assertTrue(controller.checkVisibility("cacheSucc"));
    }

    @Test
    public void whenNoLocales_localeSelectIsDisabled(){
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // Just in case make sure the locale doesn't exist
        controller.deleteLocale("Oof");
        assertFalse(controller.checkVisibility("deleteError"));

        assertFalse(controller.checkEnabled("locale3"));
        assertTrue(controller.checkText("locale3","No locales have been registered yet..."));
    }

    @Test
    public void whenNoLocale_showLocaleError(){
        controller.navigate("http://localhost:" + port + "/");
        controller.waitForLoad("preloader");

        // Just in case make sure the locale doesn't exist
        controller.deleteLocale("Oof");
        assertFalse(controller.checkVisibility("deleteError"));

        assertFalse(controller.checkEnabled("locale3"));
        controller.searchByLocale(null, null, null);
        assertTrue(controller.checkVisibility("error3"));
    }
}
