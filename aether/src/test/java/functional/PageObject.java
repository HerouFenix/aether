package functional;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.lang.Nullable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PageObject {
    private WebDriver driver;
    private Map<String, Object> vars;
    JavascriptExecutor js;

    public PageObject() {
        System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver"); //Linux Style
        driver = new FirefoxDriver();
        js = (JavascriptExecutor) driver;
        vars = new HashMap<String, Object>();
    }

    public void tear() {
        driver.quit();
    }

    public void navigate(String url) {
        driver.get(url);
    }

    public boolean checkVisibility(String id) {
        return driver.findElement(By.id(id)).isDisplayed();
    }
    public boolean checkEnabled(String id) {
        return driver.findElement(By.id(id)).isEnabled();
    }

    public int getNumberOfSelectOptions(String id) {
        Select se = new Select(driver.findElement(By.id(id)));
        List<WebElement> l = se.getOptions();
        return l.size();
    }

    public boolean checkText(String id, String text) {
        return driver.findElement(By.id(id)).getText().equals(text);
    }

    public void waitForLoad(String loader) {
        // Had to add these exceptions due to the fact that the preloader gets destroyed sometimes faster than the page is created
        try {
            if (driver.findElements(By.id(loader)).size() > 0 && driver.findElement(By.id(loader)).isDisplayed()) {
                WebDriverWait wait2 = new WebDriverWait(driver, 120);
                wait2.until(ExpectedConditions.invisibilityOf(driver.findElement(By.id(loader))));
            }
        } catch (StaleElementReferenceException e) {
            return;
        } catch (NoSuchElementException e){
            return;
        }
    }


    public void searchByCoordinates(@Nullable String lat, @Nullable String lon, @Nullable String api, @Nullable String reqType) {
        if (lat != null) {
            driver.findElement(By.id("lat2")).click();
            if (lat.equals("")) {
                driver.findElement(By.id("lat2")).clear();
            } else {
                driver.findElement(By.id("lat2")).clear();
                driver.findElement(By.id("lat2")).sendKeys(lat);
            }
        }

        if (lon != null) {
            driver.findElement(By.id("lon2")).click();
            if (lon.equals("")) {
                driver.findElement(By.id("lon2")).clear();
            } else {
                driver.findElement(By.id("lon2")).clear();
                driver.findElement(By.id("lon2")).sendKeys(lon);
            }
        }

        if (api != null) {
            Select apiSelect = new Select(driver.findElement(By.id("api2")));
            apiSelect.selectByVisibleText(api);
        }

        if (reqType != null) {
            Select reqSelect = new Select(driver.findElement(By.id("reqType2")));
            reqSelect.selectByVisibleText(reqType);
        }

        driver.findElement(By.id("search2")).sendKeys(Keys.SPACE);
    }

    public void searchByLocale(@Nullable String locale, @Nullable String api, @Nullable String reqType) {
        if (locale != null) {
            Select localeSelect = new Select(driver.findElement(By.id("locale3")));
            localeSelect.selectByVisibleText(locale);
        }

        if (api != null) {
            Select apiSelect = new Select(driver.findElement(By.id("api3")));
            apiSelect.selectByVisibleText(api);
        }

        if (reqType != null) {
            Select reqSelect = new Select(driver.findElement(By.id("reqType3")));
            reqSelect.selectByVisibleText(reqType);
        }

        driver.findElement(By.id("search3")).sendKeys(Keys.SPACE);
    }

    public void addNewLocale(@Nullable String locale, @Nullable String lat, @Nullable String lon) {
        if (lat != null) {
            driver.findElement(By.id("newLat")).click();
            if (lat.equals("")) {
                driver.findElement(By.id("newLat")).clear();
            } else {
                driver.findElement(By.id("newLat")).clear();
                driver.findElement(By.id("newLat")).sendKeys(lat);
            }
        }

        if (lon != null) {
            driver.findElement(By.id("newLon")).click();
            if (lon.equals("")) {
                driver.findElement(By.id("newLon")).clear();
            } else {
                driver.findElement(By.id("newLon")).clear();
                driver.findElement(By.id("newLon")).sendKeys(lon);
            }
        }

        if (locale != null) {
            driver.findElement(By.id("newName")).click();
            if (locale.equals("")) {
                driver.findElement(By.id("newName")).clear();
            } else {
                driver.findElement(By.id("newName")).clear();
                driver.findElement(By.id("newName")).sendKeys(locale);
            }
        }

        driver.findElement(By.id("newLocale")).sendKeys(Keys.SPACE);
    }

    public void updateLocale(@Nullable String locale, @Nullable String lat, @Nullable String lon) {
        if (lat != null) {
            driver.findElement(By.id("updateLat")).click();
            if (lat.equals("")) {
                driver.findElement(By.id("updateLat")).clear();
            } else {
                driver.findElement(By.id("updateLat")).clear();
                driver.findElement(By.id("updateLat")).sendKeys(lat);
            }
        }

        if (lon != null) {
            driver.findElement(By.id("updateLon")).click();
            if (lon.equals("")) {
                driver.findElement(By.id("updateLon")).clear();
            } else {
                driver.findElement(By.id("updateLon")).clear();
                driver.findElement(By.id("updateLon")).sendKeys(lon);
            }
        }

        if (locale != null) {
            driver.findElement(By.id("updateName")).click();
            if (locale.equals("")) {
                driver.findElement(By.id("updateName")).clear();
            } else {
                driver.findElement(By.id("updateName")).clear();
                driver.findElement(By.id("updateName")).sendKeys(locale);
            }
        }


        driver.findElement(By.id("updateLocale")).sendKeys(Keys.SPACE);
    }

    public void deleteLocale(@Nullable String locale) {
        if (locale != null) {
            driver.findElement(By.id("deleteName")).click();
            if (locale.equals("")) {
                driver.findElement(By.id("deleteName")).clear();
            } else {
                driver.findElement(By.id("deleteName")).clear();
                driver.findElement(By.id("deleteName")).sendKeys(locale);
            }
        }

        driver.findElement(By.id("deleteLocale")).sendKeys(Keys.SPACE);
    }

    public void getCacheInfo() {
        driver.findElement(By.id("cacheInfo")).sendKeys(Keys.SPACE);
    }

}
